/**
  cc_gv.c

  As funÃ§Ãµes deste arquivo, se corretamente utilizadas, permitem a
  geraÃ§Ã£o de um grafo no formato dot (graphviz). Ele imprime na saÃ­da
  de erro do programa o grafo. Para utilizar, chame gv_init no inÃ­cio
  do programa, depois chame gv_declare para declarar um nÃ³ da AST
  (passando o seu tipo - de acordo com ast.h, o pointeiro para o nÃ³ da
  AST e o seu nome). Para conectar dois nÃ³s da AST, utilize gv_connect
  e para fechar o arquivo utilize gv_close.

  AtenÃ§Ã£o: nÃ£o altere este arquivo

  Supondo que o arquivo de saÃ­da deste mÃ³dulo encontra-se no arquivo
  "saida.dot", ele pode ser visualizado da seguinte maneira:

  1 - instale o pacote graphviz
  2 - execute o comando "dot saida.dot -Tpng -o saida.png"
 */
#include <stdio.h>
#include <stdlib.h>
#include "cc_ast.h"

#define INTERNAL_OUTPUT "e3.dot"

static FILE *fp = NULL;
FILE *intfp = NULL;
extern void *comp_tree_last;

extern void *comp_tree_last;

static inline void __gv_test_valid_fp (const char *function_name)
{
  if (!fp){
    fprintf (stderr, "%s foi chamada, mas gv_init nÃ£o foi chamada antes, abort()\n", function_name);
    abort();
  }
}

static inline void __gv_test_valid_ast_pointer (const char *function_name, const char *pointer)
{
  if (!pointer){
    fprintf (stderr, "%s foi chamada com pointer = NULL\n", function_name);
    abort();
  }
}

static inline char *__gv_description_from_type (int tipo)
{
  switch (tipo){
  case AST_PROGRAMA: return "programa";
  case AST_IF_ELSE: return "ifelse";
  case AST_DO_WHILE: return "dowhile";
  case AST_WHILE_DO: return "whiledo";
  case AST_INPUT: return "input";
  case AST_OUTPUT: return "output";
  case AST_ATRIBUICAO: return "=";
  case AST_RETURN: return "return";
  case AST_BLOCO: return "block";
  case AST_ARIM_SOMA: return "+";
  case AST_ARIM_SUBTRACAO: return "-";
  case AST_ARIM_MULTIPLICACAO: return "*";
  case AST_ARIM_DIVISAO: return "/";
  case AST_ARIM_INVERSAO: return "-";
  case AST_LOGICO_E: return "&&";
  case AST_LOGICO_OU: return "||";
  case AST_LOGICO_COMP_DIF: return "!=";
  case AST_LOGICO_COMP_IGUAL: return "==";
  case AST_LOGICO_COMP_LE: return "<=";
  case AST_LOGICO_COMP_GE: return ">=";
  case AST_LOGICO_COMP_L: return "<";
  case AST_LOGICO_COMP_G: return ">";
  case AST_LOGICO_COMP_NEGACAO: return "!";
  case AST_VETOR_INDEXADO: return "[]";
  case AST_CHAMADA_DE_FUNCAO: return "call";
  case AST_SHIFT_LEFT: return "<<";
  case AST_SHIFT_RIGHT: return ">>";
  case AST_MULTI_DIM: return "[m]";

  default:
    fprintf (stderr, "%s: tipo provided is invalid here\n", __FUNCTION__);
    abort();
  }
  fprintf (stderr, "%s: should not get here\n", __FUNCTION__);
  abort();
}

/**
 * gv_init
 *
 * Esta funÃ§Ã£o deve ser chamada para inicializar o arquivo onde o
 * grafo serÃ¡ registrado. Um nome de arquivo, opcional, pode ser
 * passado como argumento para esta funÃ§Ã£o. Caso o parÃ¢metro com o
 * nome do arquivo seja NULL, a saÃ­da serÃ¡ impressa na saÃ­da de erro.
 */
void gv_init (const char *filename)
{
  //verificar se gv_init jÃ¡ foi chamada
  if (fp){
    fprintf (stderr, "%s:%d jÃ¡ foi chamada, abort()\n", __FUNCTION__, __LINE__);
    abort();

  }

  //se o nome do arquivo for vÃ¡lido, abre arquivo com esse nome para escrita
  if (filename){
    fp = fopen (filename, "w");
    if (!fp){
      fprintf (stderr, "%s:%d nÃ£o conseguiu abrir o arquivo %s para escrita\n", __FUNCTION__, __LINE__, filename);
      abort();
    }
  }else{
    fp = stderr;
  }
  fprintf (fp, "digraph G {\n");

  intfp = fopen(INTERNAL_OUTPUT, "w");
  fprintf(intfp, "digraph G {\n");
  fprintf(intfp, "  root [label=\"root\"]\n");
}

/**
 * gv_close
 *
 * Esta funÃ§Ã£o deve ser chamada para fechar o arquivo com o grafo
 * registrado. Ela deve ser chamada no final do compilador.
 */
void gv_close (void)
{
  __gv_test_valid_fp (__FUNCTION__);
  fprintf (fp, "}\n");
  fclose(fp);


  if (comp_tree_last){
    fprintf(intfp, "root -> node_%p\n", comp_tree_last);
    //fprintf(intfp, "root [label=\"root\"]\n");
  }
  fprintf(intfp, "}\n");
  fclose(intfp);
}

/**
 * gv_declare
 *
 * Esta funÃ§Ã£o deve ser chamada para declarar um nÃ³ da AST,
 * registrando esse novo nÃ³ no arquivo. Ela tem trÃªs parÃ¢metros:

 * 1/ tipo, que deve ser obrigatoriamente um dos valores das
 * constantes declaradas no arquivo iks_ast.h;

 * 2/ pointer, que deve ser um pointeiro para o nÃ³ da Ã¡rvore AST que
 * estÃ¡ sendo declarado servindo a partir de agora como identificador
 * Ãºnico do nÃ³; e

 * 3/ name, que deve ser um lexema vÃ¡lido somente se o tipo for um
 * desses trÃªs valores: AST_IDENTIFICADOR (o lexema do
 * identificador), AST_LITERAL (o lexema do literal) ou
 * AST_FUNCAO (o lexema do identificador da funÃ§Ã£o).
 */
void gv_declare (const int tipo, const void *pointer, char *name)
{
  __gv_test_valid_fp (__FUNCTION__);
  __gv_test_valid_ast_pointer (__FUNCTION__, pointer);

  char *description = NULL;

  switch (tipo){
  case AST_FUNCAO:
  case AST_IDENTIFICADOR:
  case AST_LITERAL:
    if (!name){
      fprintf (stderr, "%s: name should be not NULL\n", __FUNCTION__);
      abort();
    }
    description = name;
    break;

  case AST_PROGRAMA:
  case AST_IF_ELSE:
  case AST_DO_WHILE:
  case AST_WHILE_DO:
  case AST_INPUT:
  case AST_OUTPUT:
  case AST_ATRIBUICAO:
  case AST_RETURN:
  case AST_BLOCO:
  case AST_ARIM_SOMA:
  case AST_ARIM_SUBTRACAO:
  case AST_ARIM_MULTIPLICACAO:
  case AST_ARIM_DIVISAO:
  case AST_ARIM_INVERSAO:
  case AST_LOGICO_E:
  case AST_LOGICO_OU:
  case AST_LOGICO_COMP_DIF:
  case AST_LOGICO_COMP_IGUAL:
  case AST_LOGICO_COMP_LE:
  case AST_LOGICO_COMP_GE:
  case AST_LOGICO_COMP_L:
  case AST_LOGICO_COMP_G:
  case AST_LOGICO_COMP_NEGACAO:
  case AST_VETOR_INDEXADO:
  case AST_CHAMADA_DE_FUNCAO:
  case AST_SHIFT_LEFT:
  case AST_SHIFT_RIGHT:
  case AST_MULTI_DIM:
    if (name){
      fprintf (stderr, "%s: name should be NULL\n", __FUNCTION__);
      abort();
    }
    description = __gv_description_from_type (tipo);
    break;

  default:
    fprintf (stderr, "%s: unknow tipo provided\n", __FUNCTION__);
    abort();
  }

  fprintf (fp, "node_%p [label=\"%s\"]\n", pointer, description);
}

/**
 * gv_connect
 *
 * Esta funÃ§Ã£o deve ser utilizada para conectar dois nÃ³s da AST na
 * saÃ­da em dot. Ela recebe como parÃ¢metro ponteiros para os dois nÃ³s
 * da AST que devem ser conectados. Note que esses ponteiros servem
 * como identificadores Ãºnicos dos nÃ³s da AST.
 */
void gv_connect (const void *p1, const void *p2)
{
  __gv_test_valid_fp (__FUNCTION__);
  __gv_test_valid_ast_pointer (__FUNCTION__, p1);
  __gv_test_valid_ast_pointer (__FUNCTION__, p2);

  fprintf(fp, "node_%p -> node_%p\n", p1, p2);
}
