#include "cc_list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int nextReg = 0;
int nextLabel = 0;

iloc_list_item_t* new_iloc_item(char* opcode, int op1, int op2, int op3){
	iloc_list_item_t* item = (iloc_list_item_t*) malloc(sizeof(iloc_list_item_t));
	item->opcode = strdup(opcode);
	item->op1 = op1;
	item->op2 = op2;
	item->op3 = op3;
	item->next = NULL;
	return item;
}

iloc_list_t* new_iloc_list(){
	iloc_list_t* list = (iloc_list_t*) malloc(sizeof(iloc_list_t));
	list->first = NULL;
	list->last = NULL;
	return list;
}

void iloc_list_debug_print(iloc_list_t *l){
	iloc_list_item_t *aux = l->first;
	while(aux != NULL) {
 		printf("opcode: %s\top1: %i\top2: %i\top3: %i\t\n", aux->opcode, aux->op1, aux->op2, aux->op3);
		aux = aux->next;
	}
}

void addInstructionToList(iloc_list_t* list, iloc_list_item_t* item){
    if (list->first == NULL){
        list->first = item;
	list->last = item;
    }else {
        list->last->next = item;
        list->last = item;
    }
    return;
}

void addInstructionToListBeforeLast(iloc_list_t* list, iloc_list_item_t* item){
	iloc_list_item_t *aux = list->first;
	while(aux != NULL) {
				if (aux->next == list->last) { // is before last
					aux->next = item;
					item->next = list->last;
				}
				aux = aux->next;
    }
    return;
}

iloc_list_t* connectLists(iloc_list_t* l1, iloc_list_t* l2){
    if (l1 == NULL || l1->first == NULL) return l2;
    if (l2 == NULL || l2->first == NULL) return l1;
    l1->last->next = l2->first;
    l1->last = l2->last;
    return l1;
}

int regName(){
	return nextReg++;
}

int labelName(){
	return nextLabel++;
}

iloc_list_t* createCode(comp_tree_t* node){
    switch(node->value->type)
    {
		case AST_LITERAL:
			return InstLiteral(node);
			break;
		case AST_ARIM_SOMA:
			return InstArithmetic(node,"add");
			break;
		case AST_ARIM_SUBTRACAO:
			return InstArithmetic(node,"sub");
			break;
		case AST_ARIM_MULTIPLICACAO:
			return InstArithmetic(node,"mult");
			break;
		case AST_ARIM_DIVISAO:
	  		return InstArithmetic(node,"div");
			break;
		case AST_SHIFT_LEFT:
			return InstArithmetic(node,"lshift");
			break;
			case AST_SHIFT_RIGHT:
				return InstArithmetic(node,"rshift");
				break;
		case AST_LOGICO_COMP_DIF:
	    case AST_LOGICO_COMP_IGUAL:
	    case AST_LOGICO_COMP_LE:
	    case AST_LOGICO_COMP_GE:
	    case AST_LOGICO_COMP_L:
	    case AST_LOGICO_COMP_G:
			return InstBools(node);
			break;
		case AST_LOGICO_E:
		case AST_LOGICO_OU:
			return InstLogical(node);
			break;
		case AST_ARIM_INVERSAO:
		case AST_LOGICO_COMP_NEGACAO:
			return InstSingleBool(node);
			break;
		case AST_ATRIBUICAO:
			return InstAssignment(node);
			break;
		case AST_IDENTIFICADOR:
			return loadSingleVar(node);
			break;
		case AST_VETOR_INDEXADO:
			return loadVector(node);
			break;
		case AST_IF_ELSE:
			return InstIf(node);
			break;
		case AST_WHILE_DO:
			return InstWhile(node);
			break;
		case AST_DO_WHILE:
			return InstDoWhile(node);
			break;
		default:
			break;
    }
}

iloc_list_t* loadSingleVar(comp_tree_t* node){
	iloc_list_item_t* item;
	int resultReg = regName();
	int initOffset, offset;
	offset = node->value->item->content->offset;
	initOffset = node->value->item->content->scope;
	item = new_iloc_item("loadAI", initOffset, offset, resultReg);
	node->regVal = resultReg;
	iloc_list_t* il;
	il = new_iloc_list();
	addInstructionToList(il, item);
	return il;
}

iloc_list_t* loadVector(comp_tree_t* node){
	iloc_list_item_t* item;
	int resultReg = regName();
	int offsetResReg = regName();
	iloc_list_t* il;
	il = new_iloc_list();
	int i=0;
	il = connectLists(NULL,node->first->next->code); // adding vector expressions
	connectLists(il,node->first->code);

	int initOffset;
	initOffset = node->first->value->item->content->scope; // rbss or fp
	int curOffset = node->first->value->item->content->offset; // current offset sum with result

	int offsetCalc = calcVectorOffsetReg(node,il);
	item = new_iloc_item("addI",offsetCalc,curOffset,offsetResReg);
	addInstructionToList(il, item);
	item = new_iloc_item("loadA0",initOffset,offsetResReg,resultReg);
	node->regVal = resultReg;
	addInstructionToList(il, item);
	return il;
}

int calcVectorOffsetReg(comp_tree_t* node, iloc_list_t* il) {
	int i=0;
	int d[20] = { -1 };
	param_list_t* listDefine = node->first->value->item->content->paramList;
	param_list_t* listExps = node->first->next->paramList;
	param_list_t *aux = listExps;
	param_list_t *n = listDefine;
	/*d0 = i0;
	d1 = d0*n1 + i1 */
while (aux != NULL && n!=NULL) {
		d[i] = regName();
		iloc_list_item_t* it;
		if (i == 0) {
			it = new_iloc_item("i2i",aux->type,d[i],-1);
			addInstructionToList(il,it);
		}
		else {
			int tmpMult = regName();
			it = new_iloc_item("multI",d[i-1],n->type,tmpMult);
			addInstructionToList(il,it);
			it = new_iloc_item("add",aux->type,tmpMult,d[i]);
			addInstructionToList(il,it);
		}
		i++;
		n = n->next;
		aux = aux->next;

	}
	d[i] = regName();
	int size = node->first->value->item->content->size;
	iloc_list_item_t* item;
	item = new_iloc_item("multI",d[i-1],size,d[i]);
	addInstructionToList(il,item);
	return d[i];
}


iloc_list_t* InstAssignment(comp_tree_t* node){
	iloc_list_t* il;
	il = new_iloc_list();
	il = connectLists(NULL,node->first->next->code); // adding expression code
	if (node->first->value->type == AST_IDENTIFICADOR)
		connectLists(il,node->first->code);
	else
		connectLists(il,node->first->first->next->code); // adding multi dim exps
	iloc_list_item_t* item;
	int initOffset, offset, resultReg = regName();
	int offsetResReg = regName();
	if (node->first->value->type == AST_IDENTIFICADOR) {
		initOffset = node->first->value->item->content->scope;
		offset = node->first->value->item->content->offset;
		item = new_iloc_item("storeAI", node->first->next->regVal, initOffset, offset);
		addInstructionToList(il, item);
	}
	else if (node->first->value->type == AST_VETOR_INDEXADO) {

		initOffset = node->first->first->value->item->content->scope;
		int curoffset = node->first->first->value->item->content->offset;
		int offsetCalc = calcVectorOffsetReg(node->first,il);
		item = new_iloc_item("addI",offsetCalc,curoffset,offsetResReg);
		addInstructionToList(il, item);
		item = new_iloc_item("storeA0", node->first->next->regVal, initOffset, offsetResReg);
		addInstructionToList(il, item);
	}

	return il;
}

iloc_list_t* InstLogical(comp_tree_t* node){
	iloc_list_item_t* item;
	int resultReg = regName();
	int labelNormal = labelName();
	int labelShort = labelName();
	int labelBackToProgram = labelName();
	iloc_list_t* il = new_iloc_list();
	il = connectLists(NULL,node->first->code); // at least should compute first node
	if (node->value->type == AST_LOGICO_OU)
		item = new_iloc_item("cbr", node->first->regVal, labelShort, labelNormal);
	else if (node->value->type == AST_LOGICO_E)
		item = new_iloc_item("cbr", node->first->regVal, labelNormal, labelShort);

	addInstructionToList(il, item);
	iloc_list_item_t* inst;
	iloc_list_item_t* normalInst;
	normalInst = new_iloc_item("label",labelNormal,-1,-1);
	addInstructionToList(il,normalInst);
	connectLists(il,node->first->next->code); // should compute other part
	inst = new_iloc_item("i2i",node->first->next->regVal,resultReg,-1);
	addInstructionToList(il,inst);

	inst = new_iloc_item("jumpI",labelBackToProgram,-1,-1);
	addInstructionToList(il,inst);


	inst = new_iloc_item("label",labelShort,-1,-1);
	addInstructionToList(il,inst);

	inst = new_iloc_item("i2i",node->first->regVal,resultReg,-1);
	addInstructionToList(il,inst);

	//jump to end


	// no need to jump

	normalInst = new_iloc_item("label",labelBackToProgram,-1,-1); //back to program
	addInstructionToList(il,normalInst);
	node->regVal = resultReg;
	return il;
}

iloc_list_t* InstSingleBool(comp_tree_t* node){
	iloc_list_item_t* item;
	int resultReg = regName();

	if (node->value->type == AST_ARIM_INVERSAO) {
		item = new_iloc_item("rsubI ", node->first->regVal, 0 , resultReg);
	}
	node->regVal = resultReg;
	iloc_list_t* il;
	il = new_iloc_list();
	il = connectLists(NULL,node->first->code);
	addInstructionToList(il, item);
	return il;
}

iloc_list_t* InstArithmetic(comp_tree_t* node,char* opcode){
	iloc_list_item_t* item;
	int resultReg = regName();
	item = new_iloc_item(opcode,node->first->regVal,node->first->next->regVal,resultReg);
	node->regVal = resultReg;
	iloc_list_t* il;
	il = new_iloc_list();
	il = connectLists(NULL,node->first->code);
	connectLists(il,node->first->next->code);
	addInstructionToList(il, item);

	return il;
}

iloc_list_t* InstLiteral(comp_tree_t* node) {
	iloc_list_item_t* item;
	int op1, resultReg = regName();
	if (SIMBOLO_LITERAL_INT == node->value->item->content->type)
		op1 = node->value->item->content->value.intValue;
	else if (SIMBOLO_LITERAL_FLOAT == node->value->item->content->type)
		op1 = node->value->item->content->value.floatValue;
	else if (SIMBOLO_LITERAL_BOOL == node->value->item->content->type)
		op1 = node->value->item->content->value.boolValue;
	node->regVal = resultReg;
	item = new_iloc_item("loadI", op1, resultReg, -1);
	iloc_list_t* il;
	il = new_iloc_list();
	addInstructionToList(il, item);
	return il;
}

iloc_list_t* InstBools(comp_tree_t* node) {
	iloc_list_item_t* item;
	char* opcode;
	int resultReg = regName();
	switch(node->value->type)
	{
		case AST_LOGICO_COMP_L:
			opcode = "cmp_LT";
			break;
		case AST_LOGICO_COMP_G:
			opcode = "cmp_GT";
			break;
		case AST_LOGICO_COMP_GE:
			opcode = "cmp_GE";
			break;
		case AST_LOGICO_COMP_LE:
			opcode = "cmp_LE";
			break;
		case AST_LOGICO_COMP_IGUAL:
			opcode = "cmp_EQ";
			break;
		case AST_LOGICO_COMP_DIF:
			opcode = "cmp_NE";
			break;
	}
	item = new_iloc_item(opcode, node->first->regVal, node->first->next->regVal, resultReg);
	node->regVal = resultReg;
	iloc_list_t* il;
	il = new_iloc_list();
	il = connectLists(NULL,node->first->code);
	connectLists(il,node->first->next->code);
	addInstructionToList(il, item);
	return il;
}

iloc_list_t* InstIf(comp_tree_t* node) {
	iloc_list_t* il;
	il = new_iloc_list();
	int labelT, labelF, labelEnd;
	labelT = labelName();
	labelF = labelName();
	labelEnd = labelName();
	iloc_list_item_t *iCbr, *iJumpEnd, *iLabelT, *iLabelF, *iLabelEnd, *itemJump;
	if (node->first->value->type == AST_LOGICO_E) {
		if (node->childnodes == 3)
			itemJump = new_iloc_item("jumpI",labelF,-1,-1);
		else
			itemJump = new_iloc_item("jumpI",labelEnd,-1,-1);
	}
	else if (node->first->value->type == AST_LOGICO_OU)
		itemJump = new_iloc_item("jumpI",labelT,-1,-1);

	if (node->childnodes == 3)		// if tem else
		iCbr = new_iloc_item("cbr", node->first->regVal, labelT, labelF);
	else							// if nao tem else
		iCbr = new_iloc_item("cbr", node->first->regVal, labelT, labelEnd);
	iJumpEnd = new_iloc_item("jumpI", labelEnd, -1, -1);
	iLabelT = new_iloc_item("label", labelT, -1, -1);
	iLabelF = new_iloc_item("label", labelF, -1, -1);
	iLabelEnd = new_iloc_item("label", labelEnd, -1, -1);

	// CODE:
	il = node->first->code;
	if (node->first->value->type == AST_LOGICO_E || node->first->value->type == AST_LOGICO_OU)
		addInstructionToListBeforeLast(il,itemJump);

	addInstructionToList(il, iCbr);
	addInstructionToList(il, iLabelT);
	connectLists(il, node->first->next->code);
	if (node->childnodes == 3){		// if tem else
		addInstructionToList(il, iJumpEnd);
		addInstructionToList(il, iLabelF);
		connectLists(il, node->last->code);
	}
	addInstructionToList(il, iLabelEnd);
	return il;
}

iloc_list_t* InstDoWhile(comp_tree_t* node) {
	iloc_list_t* il;
	il = new_iloc_list();
	int labelEnd, labelIni;
	labelIni = labelName();
	labelEnd = labelName();
	iloc_list_item_t *iCbr, *iLabelIni, *iLabelEnd;
	iCbr = new_iloc_item("cbr", node->first->next->regVal, labelIni, labelEnd);
	iLabelIni = new_iloc_item("label", labelIni, -1, -1);
	iLabelEnd = new_iloc_item("label", labelEnd, -1, -1);

	// CODE:
	addInstructionToList(il, iLabelIni);
	connectLists(il, node->first->code);
	connectLists(il, node->last->code);
	addInstructionToList(il, iCbr);
	addInstructionToList(il, iLabelEnd);
	return il;
}

iloc_list_t* InstWhile(comp_tree_t* node) {
	iloc_list_t* il;
	il = new_iloc_list();
	int labelT, labelEnd, labelIni;
	labelT = labelName();
	labelIni = labelName();
	labelEnd = labelName();
	iloc_list_item_t *iCbr, *iLabelT, *iLabelIni, *iLabelEnd, *iJumpIni;
	iCbr = new_iloc_item("cbr", node->first->next->regVal, labelT, labelEnd);
	iLabelIni = new_iloc_item("label", labelIni, -1, -1);
	iLabelEnd = new_iloc_item("label", labelEnd, -1, -1);
	iLabelT = new_iloc_item("label", labelT, -1, -1);
	iJumpIni = new_iloc_item("jumpI", labelIni, -1, -1);

	// CODE:
	addInstructionToList(il, iLabelIni);
	connectLists(il, node->last->code);
	addInstructionToList(il, iCbr);
	addInstructionToList(il, iLabelT);
	connectLists(il, node->first->code);
	addInstructionToList(il, iJumpIni);
	addInstructionToList(il, iLabelEnd);
	return il;
}

void writeIlocProgram(iloc_list_t* l){
		iloc_list_item_t *aux = l->first;
		while(aux != NULL) {
			if (strcmp(aux->opcode, "nop") == 0)
     			printf("nop\n");
			else if (strcmp(aux->opcode, "cbr") == 0)
     			printf("cbr r%i -> L%i, L%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "jumpI") == 0)
     			printf("jumpI -> L%i\n", aux->op1);
			else if (strcmp(aux->opcode, "jump") == 0)
     			printf("jump -> r%i\n", aux->op1);
			else if (strcmp(aux->opcode, "loadI") == 0)
     			printf("loadI %i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "load") == 0)
     			printf("load r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "loadAI") == 0){
				if (aux->op1 == 1)
     				printf("loadAI rarp, %i => r%i\n", aux->op2, aux->op3);
				else if (aux->op1 == 0){
					printf("loadAI rbss, %i => r%i\n", aux->op2, aux->op3);
				}
			}else if (strcmp(aux->opcode, "loadA0") == 0){
				if (aux->op1 == 1)
     				printf("loadAO rarp, r%i => r%i\n", aux->op2, aux->op3);
				else if (aux->op1 == 0)
					printf("loadAO r%i, rbss => r%i\n", aux->op2, aux->op3);
			}else if (strcmp(aux->opcode, "store") == 0)
     			printf("store r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "storeAI") == 0){
				if (aux->op2 == 1)
     				printf("storeAI r%i => rarp, %i\n", aux->op1, aux->op3);
				else if (aux->op2 == 0){
					printf("storeAI r%i => rbss, %i\n", aux->op1, aux->op3);
				}
			}else if (strcmp(aux->opcode, "storeA0") == 0){
				if (aux->op2 == 1)
     				printf("storeAO r%i => rarp, r%i\n", aux->op1, aux->op3);
				else if (aux->op2 == 0){}
					printf("storeAO r%i => rbss, r%i\n", aux->op1, aux->op3);
			}
			/*else if (strcmp(aux->opcode, "cstore") == 0)
     			printf("cstore r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "cstoreAI") == 0)
     			printf("cstoreAI r%i => r%i, %i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "cstoreA0") == 0)
     			printf("cstoreA0 r%i => r%i, r%i\n", aux->op1, aux->op2, aux->op3);*/
			else if (strcmp(aux->opcode, "cmp_LT") == 0)
     			printf("cmp_LT r%i, r%i -> r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "cmp_LE") == 0)
     			printf("cmp_LE r%i, r%i -> r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "cmp_EQ") == 0)
     			printf("cmp_EQ r%i, r%i -> r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "cmp_GE") == 0)
     			printf("cmp_GE r%i, r%i -> r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "cmp_GT") == 0)
     			printf("cmp_GT r%i, r%i -> r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "cmp_NE") == 0)
     			printf("cmp_NE r%i, r%i -> r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "add") == 0)
     			printf("add r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "sub") == 0)
     			printf("sub r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "mult") == 0)
     			printf("mult r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "div") == 0)
     			printf("div r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "addI") == 0)
     			printf("addI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "subI") == 0)
     			printf("subI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "rsubI") == 0)
     			printf("rsubI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "multI") == 0)
     			printf("multI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "rdivI") == 0)
     			printf("rdivI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "divI") == 0)
     			printf("divI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "lshiftI") == 0)
     			printf("lshiftI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "lshift") == 0)
     			printf("lshift r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "rshiftI") == 0)
     			printf("rshiftI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "rshift") == 0)
     			printf("rshift r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "and") == 0)
     			printf("and r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "andI") == 0)
     			printf("andI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "or") == 0)
     			printf("or r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "orI") == 0)
     			printf("orI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "xor") == 0)
     			printf("xor r%i, r%i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "xorI") == 0)
     			printf("xorI r%i, %i => r%i\n", aux->op1, aux->op2, aux->op3);
			else if (strcmp(aux->opcode, "i2i") == 0)
     			printf("i2i r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "c2c") == 0)
     			printf("c2c r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "c2i") == 0)
     			printf("c2i r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "i2c") == 0)
     			printf("i2c r%i => r%i\n", aux->op1, aux->op2);
			else if (strcmp(aux->opcode, "label") == 0){
     			printf("L%i: ", aux->op1);
				if (aux->next == NULL || strcmp(aux->next->opcode, "label") == 0)	// add nop if there are consecutive labels
					printf("nop\n");
			}
			aux = aux->next;
    	}
}
