#include "cc_misc.h"
#include "cc_dict.h"
#include "main.h"

extern struct comp_dict* symbolTable;
extern int lineNumber;

int comp_get_line_number (void)
{
	return lineNumber;
}

void yyerror (char const *mensagem)
{
	fprintf (stderr, "%s na linha %d\n", mensagem, comp_get_line_number());
}

void main_init (int argc, char **argv)
{
	symbolTable = dict_new();
	gv_init("teste.dot");
}

void main_finalize (void)
{
 	dict_free(symbolTable);
	gv_close();
}
/*
void comp_print_table (void)
{
	int i;
	for (i = 0; i < symbolTable->size; i++) {
    	if (symbolTable->data[i]) {
			comp_dict_item_t * item;
			item = symbolTable->data[i];
			while (item != NULL) {
				int* v = item->value;
    			cc_dict_etapa_1_print_entrada (item->key, *v);
    			item = item->next;
  			}
    	}
  	}
}
*/

void comp_print_table (void)
{
	int i;
	for (i = 0; i < symbolTable->size; i++) {
    	if (symbolTable->data[i]) {
			comp_dict_item_t * item;
			item = symbolTable->data[i];
			while (item != NULL) {
    			cc_dict_etapa_2_print_entrada (item->key->lexeme, item->content->lastLine, item->key->type);
    			item = item->next;
  			}
    	}
  	}
}
