/*
 *          File: stack.c
 *        Author: Robert I. Pitts <rip@cs.bu.edu>
 * Last Modified: March 7, 2000
 *         Topic: Stack - Array Implementation
 * ----------------------------------------------------------------
 *
 * This is an array implementation of a character stack.
 */

#include <stdio.h>
#include <stdlib.h>  /* for dynamic allocation */
#include "cc_stack.h"
#include "cc_dict.h"

/************************ Function Definitions **********************/

void StackInit(stackT *stackP, int maxSize)
{
  stackElementT *newContents;

  /* Allocate a new array to hold the contents. */

  newContents = (stackElementT *)malloc(sizeof(stackElementT) * maxSize);

  if (newContents == NULL) {
    fprintf(stderr, "Insufficient memory to initialize stack.\n");
    exit(1);  /* Exit, returning error code. */
  }

  stackP->contents = newContents;
  stackP->maxSize = maxSize;
  stackP->top = -1;  /* I.e., empty */
}

void StackDestroy(stackT *stackP)
{
  /* Get rid of array. */
  free(stackP->contents);

  stackP->contents = NULL;
  stackP->maxSize = 0;
  stackP->top = -1;  /* I.e., empty */
}

void StackPush(stackT *stackP)
{
  if (StackIsFull(stackP)) {
    fprintf(stderr, "Can't push element on stack: stack is full.\n");
    exit(1);  /* Exit, returning error code. */
  }
  stackElementT elem = malloc(sizeof(comp_dict_t));
  dict_new2(elem);
  /* Put information in array; update top. */

  stackP->contents[++stackP->top] = elem;
}

void StackFree(stackT *stackP)
{
  int i;
  for (i=0; i < stackP->maxSize; i++) {
    //printf("free dict %d\n",i);
    dict_free(stackP->contents[i]);
  }
  //dict_free(stackP->contents[0]);
  //free(stackP->contents);
}

void StackPop(stackT *stackP)
{
  if (StackIsEmpty(stackP)) {
    fprintf(stderr, "Can't pop element from stack: stack is empty.\n");
    exit(1);  /* Exit, returning error code. */
  }
  //dict_free(StackTop(stackP));
  stackP->top--;
  //printf("stack %d\n",stackP->top);
}

stackElementT StackTop(stackT *stackP)
{
  if (StackIsEmpty(stackP)) {
    fprintf(stderr, "Can't return top element from stack: stack is empty.\n");
    exit(1);  /* Exit, returning error code. */
  }

  return stackP->contents[stackP->top];
}

int StackIsEmpty(stackT *stackP)
{
  return stackP->top < 0;
}

int StackIsFull(stackT *stackP)
{
  return stackP->top >= stackP->maxSize - 1;
}

int isVariableDeclared(stackT *s, comp_dict_item_key_t* key, int allStack, int typeVar){
	int topAux = s->top;
	comp_dict_item_t* result;
	result = NULL;
	while (!StackIsEmpty(s)){
    //comp_dict_t* aux = (comp_dict_t *)malloc(sizeof(comp_dict_t));
    //aux = StackTop(s);

		result = dict_get(StackTop(s), key);
		if (result != NULL)
			if (typeVar == 0 || typeVar == result->content->typeVar){
				s->top = topAux;

				return 1;
			}
		if (!allStack)
			break;
    StackPop(s);
	}
	s->top = topAux;
	return 0;
}

void * getFunctionPointer(stackT *s, comp_dict_item_key_t* key){
	int topAux = s->top;
	comp_dict_item_t* result;
	result = NULL;
	while (!StackIsEmpty(s)){
		result = dict_get( StackTop(s), key);
		if (result != NULL)
			if (2 == result->content->typeVar){
				s->top = topAux;
				return result;
			}
    StackPop(s);
	}
	s->top = topAux;
	return NULL;
}

void * getPointer(stackT *s, comp_dict_item_key_t* key,int type){
	int topAux = s->top;
	comp_dict_item_t* result;
	result = NULL;
	while (!StackIsEmpty(s)){
		result = dict_get( StackTop(s), key);
		if (result != NULL)
			if (type == result->content->typeVar){
				s->top = topAux;
				return result;
			}
    StackPop(s);
	}
	s->top = topAux;
	return NULL;
}
