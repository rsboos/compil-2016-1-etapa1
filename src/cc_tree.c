// Copyright (c) 2016 Lucas Nodari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>

#include "cc_tree.h"

extern FILE *intfp;
void *comp_tree_last = NULL;

#define ERRO(MENSAGEM) { fprintf (stderr, "[cc_tree, %s] %s.\n", __FUNCTION__, MENSAGEM); abort(); }

node_value_t* make_node_val(comp_dict_item_t *item, int type) {
	node_value_t *nodeValue = (node_value_t *) malloc(sizeof(node_value_t));
	nodeValue->type = type;
	nodeValue->item = item;
	return nodeValue;
}

char* lexemeNode(comp_tree_t* node){
	if (node->value->item){
		return node->value->item->key->lexeme;
	}else{
		return NULL;
	}
}

comp_tree_t* tree_new(void){
	comp_tree_t *tree = tree_make_node(NULL);
	return tree;
}

void tree_free(comp_tree_t *tree){
	comp_tree_t *ptr = tree;
	do {
		if (ptr->first != NULL)
			tree_free(ptr->first);
		ptr = ptr->next;
		if (tree != NULL) {
			if (tree->value != NULL)
				free(tree->value);
			free(tree);
		}
		tree = ptr;
	} while(ptr != NULL);
}

comp_tree_t* tree_make_node(node_value_t *value){
	comp_tree_t *node = malloc(sizeof(comp_tree_t));
	if (!node)
		ERRO("Failed to allocate memory for tree node");
	node->size=0;
	node->value = value;
	node->semanticType = 0;
	node->regVal = -1;
	node->coerce = 0;
	node->prev = NULL;
	node->childnodes = 0;
	node->paramList = NULL;
	node->code = NULL;
	node->first = NULL;
	node->last = NULL;
	node->next = NULL;
	node->prev = NULL;

	gv_declare(node->value->type, node, lexemeNode(node));
	return node;
}

void tree_insert_node(comp_tree_t *tree, comp_tree_t *node){
	if (tree == NULL) {
		ERRO("Cannot insert node, tree is null");
		}
	if (node == NULL) {
		ERRO("Cannot insert node, node is null");
		}

	if (tree_has_child_nodes(tree)) { // does not have
		tree->first = node;
		tree->last = node;
	} else {
		node->prev = tree->last;
		tree->last->next = node;
		tree->last = node;
	}
	++tree->childnodes;

	gv_connect(tree, node);
	fprintf (intfp, "node_%p [label=\"\"]\n", tree);
	fprintf (intfp, "node_%p [label=\"\"]\n", node);
	fprintf (intfp, "node_%p -> node_%p\n", tree, node);
	comp_tree_last = tree;
}

int tree_has_child_nodes(comp_tree_t *tree){
	if (tree != NULL){
		if (tree->childnodes == 0)
			return 1;
	}
	return 0;
}

comp_tree_t* tree_make_unary_node(node_value_t *value, comp_tree_t *node){
	comp_tree_t *newnode = tree_make_node(value);
	tree_insert_node(newnode,node);
	return newnode;
}

comp_tree_t* tree_make_binary_node(node_value_t *value, comp_tree_t *node1, comp_tree_t *node2){
	comp_tree_t *newnode = tree_make_node(value);
	tree_insert_node(newnode,node1);
	tree_insert_node(newnode,node2);
	return newnode;
}

comp_tree_t* tree_make_ternary_node(node_value_t *value, comp_tree_t *node1, comp_tree_t *node2, comp_tree_t *node3){
	comp_tree_t *newnode = tree_make_node(value);
	tree_insert_node(newnode,node1);
	tree_insert_node(newnode,node2);
	tree_insert_node(newnode,node3);
	return newnode;
}

static void print_spaces(int num){
	while (num-->0)
		putc(' ',stdout);
}

static void tree_debug_print_node(comp_tree_t *tree, int spacing){
	if (tree == NULL) return;
	print_spaces(spacing);
	printf("%p(%d): %p\n",tree,tree->childnodes,tree->value);
}

static void tree_debug_print_s(comp_tree_t *tree, int spacing){
	if (tree == NULL) return;

	comp_tree_t *ptr = tree;
	do {
		tree_debug_print_node(ptr,spacing);
		if (ptr->first != NULL)
			tree_debug_print_s(ptr->first,spacing+1);
		ptr = ptr->next;
	} while(ptr != NULL);
}

void tree_debug_print(comp_tree_t *tree){
	tree_debug_print_s(tree,0);
}


int typeSystem(comp_tree_t* t1, comp_tree_t* t2){
	if (t1->semanticType == t2->semanticType)
		return t1->semanticType;
	else if (t1->semanticType == IKS_CHAR || t2->semanticType == IKS_CHAR){
		printf("Impossible coercion of type char - %d", IKS_ERROR_CHAR_TO_X);
		exit(IKS_ERROR_CHAR_TO_X);
	}else if (t1->semanticType == IKS_STRING || t2->semanticType == IKS_STRING){
		printf("Impossible coercion of type string - %d", IKS_ERROR_STRING_TO_X);
		exit(IKS_ERROR_STRING_TO_X);
	}else if (t1->semanticType == IKS_INT){
		if (t2->semanticType == IKS_FLOAT){
			t1->coerce = 1;			// INT PRA FLOAT
			return IKS_FLOAT;
		}
		if (t2->semanticType == IKS_BOOL){
			t2->coerce = 1;			// BOOL PRA INT
			return IKS_INT;
		}
	}else if (t1->semanticType == IKS_FLOAT){
		if (t2->semanticType == IKS_INT){
			t2->coerce = 1;			// INT PRA FLOAT
			return IKS_FLOAT;
		}
		if (t2->semanticType == IKS_BOOL){
			t2->coerce = 1;			// BOOL PRA FLOAT
			return IKS_FLOAT;
		}
	}else if (t1->semanticType == IKS_BOOL){
		if (t2->semanticType == IKS_INT){
			t1->coerce = 1;			// BOOL PRA INT
			return IKS_INT;
		}
		if (t2->semanticType == IKS_FLOAT){
			t1->coerce = 1;			// BOOL PRA FLOAT
			return IKS_FLOAT;
		}
	}
}

void coercion(int expectedType, comp_tree_t* t){

	if (expectedType == t->semanticType)
		return;
	else if ((expectedType == IKS_CHAR || expectedType == IKS_STRING) && !(t->semanticType == IKS_CHAR || t->semanticType == IKS_STRING)) {
		printf("Wrong type assigned to variable - %d", IKS_ERROR_WRONG_TYPE);
		exit(IKS_ERROR_WRONG_TYPE);
	}
	else if (t->semanticType == IKS_CHAR){
		printf("Impossible coercion of type char - %d", IKS_ERROR_CHAR_TO_X);
		exit(IKS_ERROR_CHAR_TO_X);
	}else if (t->semanticType == IKS_STRING){
		printf("Impossible coercion of type string - %d", IKS_ERROR_STRING_TO_X);
		exit(IKS_ERROR_STRING_TO_X);
	}else
		t->coerce = 1;
}

void verifyReturn(int expectedType, comp_tree_t* t) {
	if (expectedType == t->semanticType)
		return;
	else if ((expectedType == IKS_CHAR || expectedType == IKS_STRING) && !(t->semanticType == IKS_CHAR || t->semanticType == IKS_STRING)) {
		printf("Wrong parameter type return - %d",  IKS_ERROR_WRONG_PAR_RETURN);
		exit(IKS_ERROR_WRONG_PAR_RETURN);
	}
	else if (t->semanticType == IKS_CHAR){
		printf("Impossible coercion of type char - %d", IKS_ERROR_CHAR_TO_X);
		exit(IKS_ERROR_CHAR_TO_X);
	}else if (t->semanticType == IKS_STRING){
		printf("Impossible coercion of type string - %d", IKS_ERROR_STRING_TO_X);
		exit(IKS_ERROR_STRING_TO_X);
	}else
		t->coerce = 1;
}
