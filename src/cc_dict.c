// Copyright (c) 2016 Lucas Nodari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "cc_dict.h"

#define ERRO(MENSAGEM) { fprintf (stderr, "[cc_dict, %s] %s.\n", __FUNCTION__, MENSAGEM); abort(); }


// one-at-a-time-hash, Jenkins, 2006
static int generate_hash(char *key, int limit)
{
  unsigned int hash;
  for (hash = 0; *key != '\0'; ++key) {
    hash += *key;
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash % limit;
}

static comp_dict_item_t *dict_item_new()
{
  comp_dict_item_t *item = malloc(sizeof(comp_dict_item_t));
  item->key = NULL;
  item->content = NULL;
  item->next = NULL;
  return item;
}

static void dict_item_free_item(comp_dict_item_t * item)
{
	if(item == NULL) return;
	if(item->content->type == SIMBOLO_LITERAL_STRING)
		free(item->content->value.stringValue);
	if(item->content->type == SIMBOLO_IDENTIFICADOR)
		free(item->content->value.identificadorValue);
  param_list_free(item->content->paramList);
	free(item->key->lexeme);
	free(item->content);
	free(item->key);
  	free(item);
}

static void dict_item_free(comp_dict_item_t * item)
{
  if (item == NULL)
    return;

  comp_dict_item_t *ptr = item;
  while (ptr != NULL) {
    ptr = item->next;
    dict_item_free_item(item);
    item = ptr;
  }

}

static void *dict_item_remove(comp_dict_item_t * target, comp_dict_item_key_t * key)
{
  comp_dict_item_t *prev = target;
  while (target != NULL && target->key != NULL) {
    if (strcmp(target->key->lexeme, key->lexeme) == 0 && key->type == target->key->type)
      break;
    prev = target;
    target = target->next;
  }
  if (target == NULL)
    return NULL;                // key não existe
  prev->next = target->next;

  void *data = target->content;
  dict_item_free_item(target);
  return data;
}

static int dict_item_insert(comp_dict_item_t * target, comp_dict_item_t * new)
{
  if (target == NULL)
    return 0;
  while (target->next != NULL)
    target = target->next;
  target->next = new;

  return 1;
}

static comp_dict_item_t *dict_item_get(comp_dict_item_t * first, comp_dict_item_key_t *key)
{
  while (first != NULL && first->key != NULL) {
    if (strcmp(first->key->lexeme, key->lexeme) == 0 && key->type == first->key->type)
      return first;
    first = first->next;
  }
  return NULL;
}

static int dict_item_list_print(comp_dict_item_t * item)
{
  int qtd = 0;
  while (item != NULL) {
    ++qtd;
    printf(" %s %i %p\n", item->key->lexeme, item->key->type, item->content);
    item = item->next;
  }
  return qtd;
}

void dict_debug_print(comp_dict_t * dict)
{
  int i, l;
  int qtd = 0;
  printf("Dict [%d/%d]\n", dict->occupation, dict->size);
  for (i = 0, l = dict->size; i < l; ++i) {
    if (dict->data[i]) {
      printf("%d: %s %i %p\n", i, dict->data[i]->key->lexeme, dict->data[i]->key->type, dict->data[i]->content);
      ++qtd;
      if (dict->data[i]->next)
        qtd += dict_item_list_print(dict->data[i]->next);
    }
  }
  printf("Number of entries: %d\n", qtd);
}

comp_dict_t *dict_new()
{
  comp_dict_t *dict = malloc(sizeof(comp_dict_t));
  if (!dict) {
    ERRO("Cannot alocate memory for dict");
    return NULL;
  }
  dict->size = DICT_SIZE;
  dict->occupation = 0;
  dict->data = malloc(sizeof(comp_dict_item_t *) * dict->size);
  if (!dict->data) {
    free(dict);
    ERRO("Cannot alocate memory for dict data");
    return NULL;
  }

  int i, l = dict->size;
  for (i = 0; i < l; ++i)
    dict->data[i] = NULL;

  return dict;
}

void dict_new2(comp_dict_t *dict)
{
  if (!dict) {
    ERRO("Cannot alocate memory for dict");
    //return NULL;
  }
  dict->size = DICT_SIZE;
  dict->occupation = 0;
  dict->data = malloc(sizeof(comp_dict_item_t *) * dict->size);
  if (!dict->data) {
    free(dict);
    ERRO("Cannot alocate memory for dict data");
    //return NULL;
  }

  int i, l = dict->size;
  for (i = 0; i < l; ++i)
    dict->data[i] = NULL;

}

void dict_free(comp_dict_t * dict)
{

  int i, l;
  if (dict != NULL) {
  for (i = 0, l = dict->size; i < l; ++i) {
    if (dict->data[i]) {
      dict_item_free(dict->data[i]);
    }
  }

  free(dict->data);
  free(dict);
  }
}

void *dict_put(comp_dict_t * dict, comp_dict_item_key_t *itemKey, comp_dict_item_content_t *itemContent)
{
  if (dict == NULL || dict->data == NULL || itemKey == NULL) {
    ERRO("At least one parameter is NULL");
  }


  int hash = generate_hash(itemKey->lexeme, dict->size);

  comp_dict_item_t *newitem = dict_item_new();
  newitem->key = itemKey;
  newitem->content = itemContent;

  if (dict->data[hash] == NULL) {       // caso 1: entrada não existe na tabela, é inserido imediatamente
    dict->data[hash] = newitem;
    ++dict->occupation;
  } else {                      // caso 2: entrada existe na tabela, inserir no encadeamento

    comp_dict_item_t *exists = dict_item_get(dict->data[hash], itemKey);
    if (!exists)
      dict_item_insert(dict->data[hash], newitem);
    else{
	  exists->content->lastLine = itemContent->lastLine;
	  dict_item_free_item(newitem);
      return exists;
	}

  }
  return newitem;
}

comp_dict_item_t *dict_get(comp_dict_t * dict, comp_dict_item_key_t *key)
{
  if (dict == NULL || dict->data == NULL || key == NULL) {
    ERRO("At least one parameter is NULL");
  }


  int hash = generate_hash(key->lexeme, dict->size);
  comp_dict_item_t *item = NULL;

  if (dict->data[hash]) {
    item = dict_item_get(dict->data[hash], key);

  }
  return item;
}

void *dict_remove(comp_dict_t * dict, comp_dict_item_key_t *key)
{
  if (dict == NULL || dict->data == NULL || key == NULL) {
    ERRO("At least one parameter is NULL");
  }


  int hash = generate_hash(key->lexeme, dict->size);
  comp_dict_item_t *item = NULL;
  void *data = NULL;

  if (dict->data[hash]) {
    if (strcmp(dict->data[hash]->key->lexeme, key->lexeme) == 0) {      // chave é primeiro elemento
      item = dict->data[hash];
      data = item->content;
      dict->data[hash] = item->next;
      dict_item_free_item(item);

      if (dict->data[hash] == NULL)     // chave foi ultimo elemento da entrada
        --dict->occupation;
    } else {                    // chave não é primeiro elemento ou não existe
      data = dict_item_remove(dict->data[hash], key);
    }
  }

  return data;
}

int contSize(int typeContent) {
  int s  = 1;
  if (typeContent == IKS_INT)
    s = 4;
  else if (typeContent == IKS_FLOAT)
    s = 8;
  return s;
}

int contentSize(int typeContent, int typeVar, int sizeArray){
	int s = 1;								// size of char and bool

	if (typeContent == IKS_INT)
		s = 4;
	else if (typeContent == IKS_FLOAT)
		s = 8;

	if (typeVar == ARRAY_DECLARATION || typeContent == IKS_STRING)
		s *= sizeArray;

	return s;
}

void *insertInSymbolTable(comp_dict_t* dict, int typeContent, int typeVar, int lastLine, char* identVal, int sizeArray) {
	comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
	comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
 	itemKey->lexeme = identVal;
 	itemKey->type = SIMBOLO_IDENTIFICADOR;
	itemContent->size = contSize(typeContent);
 	itemContent->lastLine = lastLine;
	itemContent->paramList = NULL;
	itemContent->typeVar = typeVar;
	itemContent->typeContent = typeContent;
 	itemContent->type = SIMBOLO_IDENTIFICADOR;
 	itemContent->value.identificadorValue = identVal;
 	return dict_put(dict, itemKey, itemContent);
}

param_list_t* addParam(param_list_t *l, int type){
	if (l == NULL){
		l = (param_list_t *)malloc(sizeof(param_list_t));
		l->type = type;
		l->next = NULL;
	}else{
		param_list_t *aux = l;
		while(aux->next != NULL)
			aux = aux->next;
		aux->next = (param_list_t *)malloc(sizeof(param_list_t));
		aux->next->type = type;
		aux->next->next = NULL;
	}
	return l;
}

void param_list_debug_print(param_list_t *l){
	if (l == NULL)
    printf("Empty list\n");
	else{
		param_list_t *aux = l;

		while(aux != NULL) {
      printf("Element: %d\n",aux->type);
			aux = aux->next;
    }
  }
}
void param_list_free(param_list_t *l){
	if (l == NULL) {
  }
	else{
		param_list_t *aux = l;
    param_list_t *next;
		while(aux != NULL) {
      next = aux->next;
      free(aux);
      aux = next;
    }
  }
}
int param_list_get_size(param_list_t *l){
  int ret=0;
  if (l == NULL)
    return 0;
  else{
		param_list_t *aux = l;

		while(aux != NULL) {
      ret++;
      aux = aux->next;
    }
    return ret;
  }
}

void compareParamList(param_list_t* input, param_list_t* expected){
	while (1){
		if (input == NULL && expected == NULL){
			return;
		}
    else if (param_list_get_size(input) < param_list_get_size(expected)) {
      printf("Missing arguments - %d", IKS_ERROR_MISSING_ARGS);
			exit(IKS_ERROR_MISSING_ARGS);
    }
    else if (param_list_get_size(input) > param_list_get_size(expected)) {
      printf("Too many arguments - %d", IKS_ERROR_EXCESS_ARGS);
			exit(IKS_ERROR_EXCESS_ARGS);
    }
		else if (input->type == expected->type){
			input = input->next;
			expected = expected->next;
		}else if (input->type != expected->type){
      if (input->type == IKS_CHAR || input->type == IKS_STRING || expected->type == IKS_CHAR || expected->type == IKS_STRING){
        printf("Incompatible argument type - %d", IKS_ERROR_WRONG_TYPE_ARGS);
  			exit(IKS_ERROR_WRONG_TYPE);
    	}
      else {
        input = input->next;
  			expected = expected->next;
        // add coercion
      }
    }
	}
}
