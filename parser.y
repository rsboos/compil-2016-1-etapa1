 	/*
	  Grupo 04
		Luis Felipe Polo
		Rodrigo Augusto Scheller Boos
	*/
	%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "cc_list.h"
	#include "cc_ast.h"
	#include "cc_tree.h"
	#include "cc_stack.h"
	#include "main.h"
	extern stackT s;
	comp_dict_item_t* fItem;
	int inFunctionType;
	iloc_list_t* code;
	int globalOffset = 0;
	int localOffset = 0;
	%}

	%union {
		struct comp_dict_item* valor_simbolo_lexico;
		struct comp_tree* ast;
		int TK_PR_TYPE;
		struct param_list* ParamList;
	}

	%type<ast> p
	%type<ast> program NEW_TYPE_DECLARATION GLOBAL_VAR_DECLARATION FUNCTION TK_LIT LOCAL_VAR_DECLARATION_INITIALIZER
	%type<ast> COMMAND_BLOCK_BODY ARGUMENTS INDEX_VECTOR SIMPLE_COMMAND SIMPLE_COMMAND_BODY SPECIAL_COMMAND_BODY
	%type<ast> VET_NAME LOCAL_VAR_DECLARATION
	%type<ast> IF
	%type<ast> IF_ELSE INPUT_OP OUT_OP BREAK CONTINUE COMMAND_BLOCK
	%type<ast> DO_WHILE MULTI_DIM_EXP MULTI_DIM
	%type<ast> WHILE
	%type<ast> EXPRESSION
	%type<ast> EXPRESSION_LEAF
	%type<ast> SUB_EXPRESSION
	%type<ast> RETURN_OP
	%type<ast> FUNCTION_CALL
	%type<ast> FLOW_CONTROL
	%type<ast> ASSIGNMENT
	%type<ast> FOREACH
	%type<ast> FOR
	%type<ast> SWITCH_CASE INIT_SCOPE END_SCOPE

	/* Declaração dos tokens da linguagem */
	%token TK_PR_INT
	%token TK_PR_FLOAT
	%token TK_PR_BOOL
	%token TK_PR_CHAR
	%token TK_PR_STRING
	%token TK_PR_IF
	%token TK_PR_THEN
	%token TK_PR_ELSE
	%token TK_PR_WHILE
	%token TK_PR_DO
	%token TK_PR_INPUT
	%token TK_PR_OUTPUT
	%token TK_PR_RETURN
	%token TK_PR_CONST
	%token TK_PR_STATIC
	%token TK_PR_FOREACH
	%token TK_PR_FOR
	%token TK_PR_SWITCH
	%token TK_PR_CASE
	%token TK_PR_BREAK
	%token TK_PR_CONTINUE
	%token TK_PR_CLASS
	%token TK_PR_PRIVATE
	%token TK_PR_PUBLIC
	%token TK_PR_PROTECTED
	%token TK_OC_LE
	%token TK_OC_GE
	%token TK_OC_EQ
	%token TK_OC_NE
	%token TK_OC_AND
	%token TK_OC_OR
	%token TK_OC_SL
	%token TK_OC_SR
	%token <valor_simbolo_lexico> TK_LIT_INT
	%token <valor_simbolo_lexico> TK_LIT_FLOAT
	%token <valor_simbolo_lexico> TK_LIT_FALSE
	%token <valor_simbolo_lexico> TK_LIT_TRUE
	%token <valor_simbolo_lexico> TK_LIT_CHAR
	%token <valor_simbolo_lexico> TK_LIT_STRING
	%token <valor_simbolo_lexico> TK_IDENTIFICADOR
	//%token <valor_simbolo_lexico> FUNCTION_HEADER
	%token TOKEN_ERRO

	%type<TK_PR_TYPE> TK_PR_TYPE
		%type<TK_PR_TYPE> PARAMETER
	%type<ParamList> PARAMETERS
	%type<ParamList> PARAMETERS_ALL
	//%type<dict_entry> GLOBAL_VAR_DECLARATION

  %left TK_OC_OR
	%left TK_OC_AND
  %left '<' '>' TK_OC_SL TK_OC_SR TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE
	%left '+' '-'
	%left '*' '/'
	%left '!' OPPREC

	%%
	/* Grammar rules */
	/* program: set of intercalated GLOBAL_VAR_DECLARATION, FUNCTIONS and NEW_TYPE_DECLARATION */

	p : {  code = new_iloc_list(); } INIT_STACK INIT_SCOPE program END_SCOPE {
		if ($4 != NULL) {
		$$ = tree_make_unary_node(make_node_val(NULL,AST_PROGRAMA), $4);
		tree_free($$);
		//StackFree(&s);
		$$->code = $4->code;
		//iloc_list_debug_print($$->code);
		writeIlocProgram($$->code);
		//printf("Exiting with no semantic errors");
		exit(IKS_SUCCESS);
	}  };


	program: 	NEW_TYPE_DECLARATION program { $$ = $2; } |
				GLOBAL_VAR_DECLARATION program { $$ = $2; } |
				FUNCTION program {
					$$ = $1;

					if ($2 != NULL)
						tree_insert_node($$,$2);
					} | {  $$ = NULL;  } ;

	/* Auxiliary rules */

	INIT_STACK : { StackInit(&s, 100); };
	INIT_SCOPE : {   StackPush(&s); } ;

	END_SCOPE : { /*printf("Stack s: %d\n",s.top);*/ StackPop(&s); } ;

	/* Auxiliary rules */

	ENCAPSULATION : TK_PR_PRIVATE | TK_PR_PUBLIC | TK_PR_PROTECTED;

	TK_PR_TYPE : TK_PR_INT { $$ = IKS_INT; } |
							 TK_PR_FLOAT { $$ = IKS_FLOAT; } |
							 TK_PR_BOOL { $$ = IKS_BOOL; } |
							 TK_PR_CHAR { $$ = IKS_CHAR; } |
							 TK_PR_STRING { $$ = IKS_STRING; } ;

	IS_STATIC : TK_PR_STATIC | ;

	TK_LIT : TK_LIT_INT { $$ = tree_make_node(make_node_val($1, AST_LITERAL));  } |
				 TK_LIT_FLOAT { $$ = tree_make_node(make_node_val($1, AST_LITERAL));  } |
				 TK_LIT_FALSE { $$ = tree_make_node(make_node_val($1, AST_LITERAL));  } |
				 TK_LIT_TRUE { $$ = tree_make_node(make_node_val($1, AST_LITERAL));  } |
				 TK_LIT_CHAR { $$ = tree_make_node(make_node_val($1, AST_LITERAL));  } |
				 TK_LIT_STRING { $$ = tree_make_node(make_node_val($1, AST_LITERAL));  };

/* New type. */

NEW_TYPE_DECLARATION : TK_PR_CLASS TK_IDENTIFICADOR '[' FIELD_LIST ']' ';' { $$ = NULL; };

FIELD_LIST :  FIELD_LIST ':' FIELD | FIELD;

FIELD : ENCAPSULATION TK_PR_TYPE TK_IDENTIFICADOR;
/* End New Type */

/* Global Variables */

MULTI_DIM : TK_LIT_INT {  $$ = tree_make_node(make_node_val(NULL,AST_MULTI_DIM)); $$->size = $1->content->value.intValue;

	$$->paramList = NULL;
$$->paramList = addParam($$->paramList,$1->content->value.intValue);
}
| MULTI_DIM ',' TK_LIT_INT {
														 $$->size *= $3->content->value.intValue;

$$->paramList = addParam($$->paramList,$3->content->value.intValue);
  };

GLOBAL_VAR_DECLARATION : IS_STATIC TK_PR_TYPE TK_IDENTIFICADOR '[' MULTI_DIM ']' ';'
{
	$$ = NULL;
	if (isVariableDeclared(&s, $3->key, 1, ALL_TYPES)) {
		printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
		exit(IKS_ERROR_DECLARED);
	}
	else
		insertInSymbolTable(StackTop(&s), $2, ARRAY_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);

		$3->content->scope = 0;
		$3->content->offset = globalOffset;
		globalOffset+= contSize($2)*$5->size;
		$3->content->paramList = $5->paramList;
	}
	|
	 IS_STATIC TK_PR_TYPE TK_IDENTIFICADOR ';' {
		 $$ = NULL;
							if (isVariableDeclared(&s, $3->key, 1, ALL_TYPES)) {
								printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
								exit(IKS_ERROR_DECLARED);
							}
							else
								insertInSymbolTable(StackTop(&s), $2, VARIABLE_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
						$3->content->scope = 0;
						$3->content->offset = globalOffset;
						globalOffset+= 	contSize($2);
						}
						| IS_STATIC TK_IDENTIFICADOR TK_IDENTIFICADOR '[' MULTI_DIM ']' ';' { $$ = NULL; } |
						IS_STATIC TK_IDENTIFICADOR TK_IDENTIFICADOR ';' { $$ = NULL; } ; /* single var */


/* Function declaration */

FUNCTION : IS_STATIC TK_PR_TYPE TK_IDENTIFICADOR {
if (isVariableDeclared(&s, $3->key, 1, ALL_TYPES)) {
	printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
	exit(IKS_ERROR_DECLARED);
}
else
	fItem = insertInSymbolTable(StackTop(&s), $2, FUNCTION_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
	inFunctionType = fItem->content->typeContent;
 	} INIT_SCOPE '(' PARAMETERS_ALL ')' {
			fItem->content->paramList = $7;
		} '{' COMMAND_BLOCK_BODY '}' END_SCOPE {
	$$ = tree_make_node(make_node_val($3,AST_FUNCAO));

	if ($11 != NULL) {
		tree_insert_node($$,$11);
		$$->code = $11->code;
	}
	};

PARAMETERS_ALL : PARAMETERS { $$ = $1; } | { $$ = NULL; };
PARAMETERS: PARAMETERS ',' PARAMETER { $$ = addParam($$,$3);  } |
						PARAMETER { $$ = NULL; $$ = addParam($$,$1); };


PARAMETER: IS_CONST TK_PR_TYPE TK_IDENTIFICADOR {
	$$ = $2;
if (isVariableDeclared(&s, $3->key, 0, ALL_TYPES)) {
	printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
	exit(IKS_ERROR_DECLARED);
}
else
	insertInSymbolTable(StackTop(&s), $2, VARIABLE_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
};


IS_CONST: TK_PR_CONST | ;

COMMAND_BLOCK : INIT_SCOPE '{' COMMAND_BLOCK_BODY '}' END_SCOPE {
					$$ = tree_make_node(make_node_val(NULL,AST_BLOCO));
					if ($3 != NULL) {
						tree_insert_node($$,$3);
						$$->code = $3->code;
					}
					};

COMMAND_BLOCK_BODY : SIMPLE_COMMAND COMMAND_BLOCK_BODY {
												if ($1 == NULL) {
													$$ = $2;
												}
												else {
													$$ = $1;
													if ($2 != NULL) {
														tree_insert_node($$,$2);
														iloc_list_t* il;
														il = new_iloc_list();
														il = connectLists(NULL,$1->code);
														connectLists(il,$2->code);
														$$->code = il;
													}
												}

										}
										| { $$ = NULL; };

SIMPLE_COMMAND : SIMPLE_COMMAND_BODY ';' { $$ = $1; } | SPECIAL_COMMAND_BODY {$$ = NULL; } ;

/* Special commands are the ones without ";" in the end */
SPECIAL_COMMAND_BODY: CASE { $$ = NULL; };

SIMPLE_COMMAND_BODY: LOCAL_VAR_DECLARATION { $$ = $1;  } |
					ASSIGNMENT { $$ = $1; } |
					FLOW_CONTROL { $$ = $1; } |
					INPUT_OP { $$ = NULL; }  |
					OUT_OP { $$ = NULL; }  |
					RETURN_OP { $$ = $1;  } |
					FUNCTION_CALL { $$ = $1; } |
					BREAK { $$ = NULL; } |
					CONTINUE { $$ = NULL; } |
					COMMAND_BLOCK { $$ = $1; } | { $$ = NULL; };

BREAK: TK_PR_BREAK { $$ = NULL; };

CONTINUE: TK_PR_CONTINUE { $$ = NULL; };



CASE: TK_PR_CASE TK_LIT_INT ':';
/*$3->content->scope = 0;
$3->content->offset = localOffset;
localOffset+= contSize($2)*$5->size;
$3->content->paramList = $5->paramList;*/

LOCAL_VAR_DECLARATION :
TK_PR_TYPE TK_IDENTIFICADOR LOCAL_VAR_DECLARATION_INITIALIZER {
	if ($3 != NULL)
		$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($2,AST_IDENTIFICADOR)),$3);
	else
		$$ = NULL;
		if (isVariableDeclared(&s, $2->key, 0, ALL_TYPES)) {
			printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
			exit(IKS_ERROR_DECLARED);
		}
		else {
			 insertInSymbolTable(StackTop(&s), $1, VARIABLE_DECLARATION, $2->content->lastLine, $2->content->value.identificadorValue,0);
		}
			$2->content->scope = 1;
			$2->content->offset = localOffset;
			localOffset+= contSize($1); // adjust local offset
			if ($3 != NULL) {
				iloc_list_t* tmp = new_iloc_list();
				tmp =	createCode($$);
				$$->code = tmp;
			}
	} |
TK_PR_STATIC TK_PR_CONST TK_PR_TYPE TK_IDENTIFICADOR LOCAL_VAR_DECLARATION_INITIALIZER {
	if ($5 != NULL)
		$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($4,AST_IDENTIFICADOR)),$5);
	else
		$$ = NULL;
	if (isVariableDeclared(&s, $4->key, 0, ALL_TYPES)) {
			printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
			exit(IKS_ERROR_DECLARED);
		}
		else
			insertInSymbolTable(StackTop(&s), $3, VARIABLE_DECLARATION, $4->content->lastLine, $4->content->value.identificadorValue,0);
			$4->content->scope = 1;
			$4->content->offset = localOffset;
			localOffset+= 	 contSize($3);
	if ($5 != NULL) {
		iloc_list_t* tmp = new_iloc_list();
		tmp =	createCode($$);
		$$->code = tmp;
	}
	} |
TK_PR_STATIC TK_PR_TYPE TK_IDENTIFICADOR LOCAL_VAR_DECLARATION_INITIALIZER {
	if ($4 != NULL)
		$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($3,AST_IDENTIFICADOR)),$4);
	else
		$$ = NULL;
	if (isVariableDeclared(&s, $3->key, 0, ALL_TYPES)) {
			printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
			exit(IKS_ERROR_DECLARED);
		}
		else
			insertInSymbolTable(StackTop(&s), $2, VARIABLE_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
			$3->content->scope = 1;
			$3->content->offset = localOffset;
			localOffset+= 	 contSize($2);
			if ($4 != NULL) {
				iloc_list_t* tmp = new_iloc_list();
				tmp =	createCode($$);
				$$->code = tmp;
			}
	} |
TK_PR_CONST TK_PR_TYPE TK_IDENTIFICADOR LOCAL_VAR_DECLARATION_INITIALIZER {
	if ($4 != NULL)
		$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($3,AST_IDENTIFICADOR)),$4);
	else
		$$ = NULL;
	if (isVariableDeclared(&s, $3->key, 0, ALL_TYPES)) {
			printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
			exit(IKS_ERROR_DECLARED);
		}
		else
			insertInSymbolTable(StackTop(&s), $2, VARIABLE_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
			$3->content->scope = 1;
			$3->content->offset = localOffset;
			localOffset+= 	 contSize($2);
	if ($4 != NULL) {
		iloc_list_t* tmp = new_iloc_list();
		tmp =	createCode($$);
		$$->code = tmp;
	}

	} |TK_PR_TYPE TK_IDENTIFICADOR '[' MULTI_DIM ']' LOCAL_VAR_DECLARATION_INITIALIZER {
		if ($6 != NULL)
			$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($2,AST_IDENTIFICADOR)),$6);
		else
			$$ = NULL;
			if (isVariableDeclared(&s, $2->key, 0, ALL_TYPES)) {
				printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
				exit(IKS_ERROR_DECLARED);
			}
			else {
				 insertInSymbolTable(StackTop(&s), $1, ARRAY_DECLARATION, $2->content->lastLine, $2->content->value.identificadorValue,0);
			}
			$2->content->scope = 1;
			$2->content->offset = localOffset;
			localOffset+= contSize($1)*$4->size;
			$2->content->paramList = $4->paramList;
			if ($6 != NULL) {
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
		} |
	TK_PR_STATIC TK_PR_CONST TK_PR_TYPE TK_IDENTIFICADOR '[' MULTI_DIM ']' LOCAL_VAR_DECLARATION_INITIALIZER {
		if ($8 != NULL)
			$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($4,AST_IDENTIFICADOR)),$8);
		else
			$$ = NULL;
		if (isVariableDeclared(&s, $4->key, 0, ALL_TYPES)) {
				printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
				exit(IKS_ERROR_DECLARED);
			}
			else
				insertInSymbolTable(StackTop(&s), $3, ARRAY_DECLARATION, $4->content->lastLine, $4->content->value.identificadorValue,0);
				$4->content->scope = 1;
				$4->content->offset = localOffset;
				localOffset+= contSize($3)*$6->size;
				$4->content->paramList = $6->paramList;
		if ($8 != NULL) {
			iloc_list_t* tmp = new_iloc_list();
			tmp =	createCode($$);
			$$->code = tmp;
		}
		} |
	TK_PR_STATIC TK_PR_TYPE TK_IDENTIFICADOR '[' MULTI_DIM ']' LOCAL_VAR_DECLARATION_INITIALIZER {
		if ($7 != NULL)
			$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($3,AST_IDENTIFICADOR)),$7);
		else
			$$ = NULL;
		if (isVariableDeclared(&s, $3->key, 0, ALL_TYPES)) {
				printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
				exit(IKS_ERROR_DECLARED);
			}
			else
				insertInSymbolTable(StackTop(&s), $2, ARRAY_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
				$3->content->scope = 1;
				$3->content->offset = localOffset;
				localOffset+= contSize($2)*$5->size;
				$3->content->paramList = $5->paramList;
				if ($7 != NULL) {
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
		} |
	TK_PR_CONST TK_PR_TYPE TK_IDENTIFICADOR '[' MULTI_DIM ']' LOCAL_VAR_DECLARATION_INITIALIZER {
		if ($7 != NULL)
			$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($3,AST_IDENTIFICADOR)),$7);
		else
			$$ = NULL;
		if (isVariableDeclared(&s, $3->key, 0, ALL_TYPES)) {
				printf("Identifier already declared - %d", IKS_ERROR_DECLARED);
				exit(IKS_ERROR_DECLARED);
			}
			else
				insertInSymbolTable(StackTop(&s), $2, ARRAY_DECLARATION, $3->content->lastLine, $3->content->value.identificadorValue,0);
				$3->content->scope = 1;
				$3->content->offset = localOffset;
				localOffset+= contSize($2)*$5->size;
				$3->content->paramList = $5->paramList;
				if ($7 != NULL) {
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
		} |
 	TK_IDENTIFICADOR TK_IDENTIFICADOR { $$ = NULL; } |
	TK_PR_STATIC TK_PR_CONST TK_IDENTIFICADOR TK_IDENTIFICADOR { $$ = NULL; } |
	TK_PR_STATIC TK_IDENTIFICADOR TK_IDENTIFICADOR { $$ = NULL; } |
	TK_PR_CONST TK_IDENTIFICADOR TK_IDENTIFICADOR { $$ = NULL; } ;
/* put in AST when initialized */
LOCAL_VAR_DECLARATION_INITIALIZER : TK_OC_LE TK_IDENTIFICADOR {
	$$ = tree_make_node(make_node_val($2,AST_IDENTIFICADOR));
	if (!(isVariableDeclared(&s, $2->key, 1, ALL_TYPES))) {
		printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
		exit(IKS_ERROR_UNDECLARED);
	}
	else {
		if (isVariableDeclared(&s, $2->key, 1, ARRAY_DECLARATION)) {
			printf("Identifier should be used as Array - %d", IKS_ERROR_VECTOR);
			exit(IKS_ERROR_VECTOR);
		}
		else if (isVariableDeclared(&s, $2->key, 1, FUNCTION_DECLARATION)) {
			printf("Identifier should be used as function - %d", IKS_ERROR_FUNCTION);
			exit(IKS_ERROR_FUNCTION);
		}
	}
	} |
 		TK_OC_LE TK_LIT { $$ = $2; } | { $$ = NULL; };

MULTI_DIM_EXP : EXPRESSION {  $$ = $1; $$->code = $1->code;  $$->paramList = NULL; $$->paramList = addParam($$->paramList,$1->regVal); } |
								MULTI_DIM_EXP ',' EXPRESSION { tree_insert_node($$,$3); $$->paramList = addParam($$->paramList,$3->regVal);
									iloc_list_t* il;
									il = new_iloc_list();
									il = connectLists(NULL,$1->code);
									connectLists(il,$3->code);
									$$->code = il;
								};

INDEX_VECTOR: TK_IDENTIFICADOR '[' MULTI_DIM_EXP ']' {
	$$ = tree_make_binary_node(make_node_val(NULL,AST_VETOR_INDEXADO),tree_make_node(make_node_val($1,AST_IDENTIFICADOR)),$3);

	if (!(isVariableDeclared(&s, $1->key, 1, ALL_TYPES))) {
		printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
		exit(IKS_ERROR_UNDECLARED);
	}
	else {
		if (isVariableDeclared(&s, $1->key, 1, VARIABLE_DECLARATION)) {
			printf("Identifier should be used as simple variable - %d", IKS_ERROR_VARIABLE);
			exit(IKS_ERROR_VARIABLE);
		}
		else if (isVariableDeclared(&s, $1->key, 1, FUNCTION_DECLARATION)) {
			printf("Identifier should be used as function - %d", IKS_ERROR_FUNCTION);
			exit(IKS_ERROR_FUNCTION);
		}
	}
	comp_dict_item_t* it = getPointer(&s, $1->key,ARRAY_DECLARATION);
	$$->semanticType = it->content->typeContent;
	$1->content->size = it->content->size;
	//printf("size of: %d\n",$1->content->size);


	};

ASSIGNMENT : TK_IDENTIFICADOR '=' EXPRESSION {
	$$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),tree_make_node(make_node_val($1,AST_IDENTIFICADOR)),$3);
	if (!(isVariableDeclared(&s, $1->key, 1, ALL_TYPES))) {
		printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
		exit(IKS_ERROR_UNDECLARED);
	}
	else {
		if (isVariableDeclared(&s, $1->key, 1, ARRAY_DECLARATION)) {
			printf("Identifier should be used as Array - %d", IKS_ERROR_VECTOR);
			exit(IKS_ERROR_VECTOR);
		}
		else if (isVariableDeclared(&s, $1->key, 1, FUNCTION_DECLARATION)) {
			printf("Identifier should be used as function - %d", IKS_ERROR_FUNCTION);
			exit(IKS_ERROR_FUNCTION);
		}
	}
comp_dict_item_t* it = getPointer(&s, $1->key,VARIABLE_DECLARATION);
coercion(it->content->typeContent,$3);

iloc_list_t* tmp = new_iloc_list();
tmp =	createCode($$);
$$->code = tmp;

	} |
 INDEX_VECTOR '=' EXPRESSION {
	 $$ = tree_make_binary_node(make_node_val(NULL,AST_ATRIBUICAO),$1,$3);
	 coercion($1->semanticType,$3);
	 iloc_list_t* tmp = new_iloc_list();
	 tmp =	createCode($$);
	 $$->code = tmp;
 } |
  TK_IDENTIFICADOR '!' TK_IDENTIFICADOR '=' EXPRESSION {
		$$ = tree_make_ternary_node(make_node_val(NULL,AST_ATRIBUICAO),
		tree_make_node(make_node_val($1,AST_IDENTIFICADOR)),
		tree_make_node(make_node_val($3,AST_IDENTIFICADOR)),
		$5); };

INPUT_OP: TK_PR_INPUT EXPRESSION { $$ = NULL; };

OUT_OP: TK_PR_OUTPUT EXPRESSION_LIST { $$ = NULL; };

EXPRESSION_LIST : EXPRESSION_LIST ',' EXPRESSION | EXPRESSION;

FUNCTION_CALL : TK_IDENTIFICADOR '(' ARGUMENTS ')' {
	$$ =  tree_make_binary_node(make_node_val(NULL,AST_CHAMADA_DE_FUNCAO),tree_make_node(make_node_val($1,AST_IDENTIFICADOR)),$3);
	if (!(isVariableDeclared(&s, $1->key, 1, ALL_TYPES))) {
		printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
		exit(IKS_ERROR_UNDECLARED);
	}
	else {
		if (isVariableDeclared(&s, $1->key, 1, ARRAY_DECLARATION)) {
			printf("Identifier should be used as Array - %d", IKS_ERROR_VECTOR);
			exit(IKS_ERROR_VECTOR);
		}
		else if (isVariableDeclared(&s, $1->key, 1, VARIABLE_DECLARATION)) {
			printf("Identifier should be used as variable - %d", IKS_ERROR_VARIABLE);
			exit(IKS_ERROR_VARIABLE);
		}
	}
	fItem = getPointer(&s, $1->key,FUNCTION_DECLARATION);
	compareParamList($3->paramList,fItem->content->paramList);
	$$->semanticType = fItem->content->typeContent;
	} |

	TK_IDENTIFICADOR '(' ')' { $$ = tree_make_unary_node(make_node_val(NULL,AST_CHAMADA_DE_FUNCAO),tree_make_node(make_node_val($1,AST_IDENTIFICADOR)));
		if (!(isVariableDeclared(&s, $1->key, 1, ALL_TYPES))) {
			printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
			exit(IKS_ERROR_UNDECLARED);
		}
		else {
			if (isVariableDeclared(&s, $1->key, 1, ARRAY_DECLARATION)) {
				printf("Identifier should be used as Array - %d", IKS_ERROR_VECTOR);
				exit(IKS_ERROR_VECTOR);
			}
			else if (isVariableDeclared(&s, $1->key, 1, VARIABLE_DECLARATION)) {
				printf("Identifier should be used as variable - %d", IKS_ERROR_VARIABLE);
				exit(IKS_ERROR_VARIABLE);
			}
		}
		fItem = getPointer(&s, $1->key,FUNCTION_DECLARATION);
		compareParamList(NULL,fItem->content->paramList);
		$$->semanticType = fItem->content->typeContent;
		};

ARGUMENTS: ARGUMENTS ',' EXPRESSION { tree_insert_node($$, $3); $$->paramList = addParam($$->paramList,$$->semanticType); } |
						EXPRESSION { $$ == $1; $$->paramList = NULL; $$->paramList = addParam($$->paramList,$$->semanticType); };

RETURN_OP: TK_PR_RETURN EXPRESSION {
	$$ = tree_make_unary_node(make_node_val(NULL,AST_RETURN),$2);
	verifyReturn(inFunctionType,$2);
	$$->code = $2->code;
};

FLOW_CONTROL: IF { $$ = $1; } |
 							IF_ELSE { $$ = $1; } |
							FOREACH { $$ = $1; } |
							FOR { $$ = $1; } |
							SWITCH_CASE { $$ = $1; } |
							WHILE { $$ = $1; } |
							DO_WHILE { $$ = $1; };

IF : TK_PR_IF '(' EXPRESSION ')' TK_PR_THEN SIMPLE_COMMAND {
$$ = tree_make_binary_node(make_node_val(NULL, AST_IF_ELSE), $3, $6);
$$->code = createCode($$);
//iloc_list_debug_print($$->code);
};

IF_ELSE : TK_PR_IF '(' EXPRESSION ')' TK_PR_THEN SIMPLE_COMMAND TK_PR_ELSE SIMPLE_COMMAND {
$$ = tree_make_ternary_node(make_node_val(NULL, AST_IF_ELSE), $3, $6, $8);
$$->code = createCode($$);
//iloc_list_debug_print($$->code);
};

FOREACH : TK_PR_FOREACH '(' TK_IDENTIFICADOR ':' EXPRESSION_LIST ')' SIMPLE_COMMAND { $$ = NULL; };

COMMAND_LIST: COMMAND_LIST ',' SIMPLE_COMMAND | SIMPLE_COMMAND;

FOR: TK_PR_FOR '(' COMMAND_LIST ':' EXPRESSION ':' COMMAND_LIST ')' SIMPLE_COMMAND { $$ = NULL; };

WHILE: TK_PR_WHILE '(' EXPRESSION ')' TK_PR_DO SIMPLE_COMMAND {
$$ = tree_make_binary_node(make_node_val(NULL, AST_WHILE_DO), $6, $3);
$$->code = createCode($$);
//iloc_list_debug_print($$->code);
};

DO_WHILE: TK_PR_DO SIMPLE_COMMAND TK_PR_WHILE '(' EXPRESSION ')' {
$$ = tree_make_binary_node(make_node_val(NULL, AST_DO_WHILE), $2, $5);
$$->code = createCode($$);
//iloc_list_debug_print($$->code);
};

SWITCH_CASE: TK_PR_SWITCH '(' EXPRESSION ')' SIMPLE_COMMAND { $$ = NULL; };

	/* Expressions */

	VET_NAME : TK_IDENTIFICADOR { $$ = tree_make_node(make_node_val($1,AST_IDENTIFICADOR));
		if (!(isVariableDeclared(&s, $1->key, 1, ALL_TYPES))) {
			printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
			exit(IKS_ERROR_UNDECLARED);
		}
		else {
			if (isVariableDeclared(&s, $1->key, 1, VARIABLE_DECLARATION)) {
				printf("Identifier should be used as simple variable - %d", IKS_ERROR_VARIABLE);
				exit(IKS_ERROR_VECTOR);
			}
			else if (isVariableDeclared(&s, $1->key, 1, FUNCTION_DECLARATION)) {
				printf("Identifier should be used as function - %d", IKS_ERROR_FUNCTION);
				exit(IKS_ERROR_FUNCTION);
			}
		}
		comp_dict_item_t* it = getPointer(&s, $1->key,ARRAY_DECLARATION);
		$1->content->size = it->content->size;
		$$->semanticType = it->content->typeContent;
	};
	EXPRESSION_LEAF: TK_IDENTIFICADOR { $$ = tree_make_node(make_node_val($1, AST_IDENTIFICADOR));
		if (!(isVariableDeclared(&s, $1->key, 1, ALL_TYPES))) {
			printf("Identifier undeclared - %d", IKS_ERROR_UNDECLARED);
			exit(IKS_ERROR_UNDECLARED);
		}
		else {
			if (isVariableDeclared(&s, $1->key, 1, ARRAY_DECLARATION)) {
				printf("Identifier should be used as Array - %d", IKS_ERROR_VECTOR);
				exit(IKS_ERROR_VECTOR);
			}
			else if (isVariableDeclared(&s, $1->key, 1, FUNCTION_DECLARATION)) {
				printf("Identifier should be used as function - %d", IKS_ERROR_FUNCTION);
				exit(IKS_ERROR_FUNCTION);
			}
		}

		comp_dict_item_t* it = getPointer(&s, $1->key,VARIABLE_DECLARATION);
		$$->semanticType = it->content->typeContent;
		iloc_list_t* tmp = new_iloc_list();
		tmp =	createCode($$);
		$$->code = tmp;
	}
| TK_LIT_INT { $$ = tree_make_node(make_node_val($1, AST_LITERAL));
	iloc_list_t* tmp = new_iloc_list();
	tmp =	createCode($$);
	$$->code = tmp;

	$$->semanticType = IKS_INT;

							  }
					| TK_LIT_FLOAT { $$ = tree_make_node(make_node_val($1, AST_LITERAL));
						iloc_list_t* tmp = new_iloc_list();
						tmp =	createCode($$);
						$$->code = tmp;
						$$->semanticType = IKS_FLOAT;
					}
					| TK_LIT_FALSE { $$ = tree_make_node(make_node_val($1, AST_LITERAL)); $$->semanticType = IKS_BOOL; iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp; }
					| TK_LIT_TRUE { $$ = tree_make_node(make_node_val($1, AST_LITERAL)); $$->semanticType = IKS_BOOL; iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp; }
					| VET_NAME '[' MULTI_DIM_EXP ']' { $$ = tree_make_binary_node(make_node_val(NULL, AST_VETOR_INDEXADO), $1, $3); $$->semanticType = $1->semanticType;
						iloc_list_t* tmp = new_iloc_list();
						tmp =	createCode($$);
						$$->code = tmp;
					 }

					| FUNCTION_CALL { $$ = $1; $$->semanticType = $1->semanticType; };

	/* complete list of operators with bool exp */
	EXPRESSION: SUB_EXPRESSION { $$ = $1;
		  }
	| EXPRESSION '+' EXPRESSION {

		$$ = tree_make_binary_node(make_node_val(NULL, AST_ARIM_SOMA), $1, $3);
	$$->semanticType = typeSystem($1,$3);
	iloc_list_t* tmp = new_iloc_list();
	tmp =	createCode($$);
	$$->code = tmp;

	}

| EXPRESSION '-' EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_ARIM_SUBTRACAO), $1, $3);
$$->semanticType = typeSystem($1,$3);
iloc_list_t* tmp = new_iloc_list();
tmp =	createCode($$);
$$->code = tmp;
}
| EXPRESSION '*' EXPRESSION {
	$$ = tree_make_binary_node(make_node_val(NULL, AST_ARIM_MULTIPLICACAO), $1, $3);
	$$->semanticType = typeSystem($1,$3);
	iloc_list_t* tmp = new_iloc_list();
	tmp =	createCode($$);
	$$->code = tmp;
	}
| EXPRESSION '/' EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_ARIM_DIVISAO), $1, $3);
$$->semanticType = typeSystem($1,$3);
iloc_list_t* tmp = new_iloc_list();
tmp =	createCode($$);
$$->code = tmp;
}
	| EXPRESSION '<' EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_COMP_L), $1, $3);
		typeSystem($1, $3);
		$$->semanticType = IKS_BOOL;
		iloc_list_t* tmp = new_iloc_list();
		tmp =	createCode($$);
		$$->code = tmp;
	}
	| EXPRESSION '>' EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_COMP_G), $1, $3);
				typeSystem($1, $3);
				$$->semanticType = IKS_BOOL;
				iloc_list_t* tmp = new_iloc_list();
				tmp =	createCode($$);
				$$->code = tmp;
        //iloc_list_debug_print($$->code);
	}
	| EXPRESSION TK_OC_SL EXPRESSION {
		$$ = tree_make_binary_node(make_node_val(NULL, AST_SHIFT_LEFT), $1, $3);
		$$->semanticType = typeSystem($1,$3);
		iloc_list_t* tmp = new_iloc_list();
		tmp =	createCode($$);
		$$->code = tmp;
	 }
		| EXPRESSION TK_OC_SR EXPRESSION {
			$$ = tree_make_binary_node(make_node_val(NULL, AST_SHIFT_RIGHT), $1, $3);
			$$->semanticType = typeSystem($1,$3);
			iloc_list_t* tmp = new_iloc_list();
			tmp =	createCode($$);
			$$->code = tmp;
		  }
				| EXPRESSION TK_OC_LE EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_COMP_LE), $1, $3);
					typeSystem($1, $3);
					$$->semanticType = IKS_BOOL;
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
				| EXPRESSION TK_OC_GE EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_COMP_GE), $1, $3);
					typeSystem($1, $3);
					$$->semanticType = IKS_BOOL;
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
				| EXPRESSION TK_OC_EQ EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_COMP_IGUAL), $1, $3);
					typeSystem($1, $3);
					$$->semanticType = IKS_BOOL;
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
				| EXPRESSION TK_OC_NE EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_COMP_DIF), $1, $3);
					typeSystem($1, $3);
					$$->semanticType = IKS_BOOL;
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
        | EXPRESSION TK_OC_OR EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_OU), $1, $3);
					typeSystem($1, $3);
					$$->semanticType = IKS_BOOL;
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;
				}
				| EXPRESSION TK_OC_AND EXPRESSION { $$ = tree_make_binary_node(make_node_val(NULL, AST_LOGICO_E), $1, $3);
					typeSystem($1, $3);
					$$->semanticType = IKS_BOOL;
					iloc_list_t* tmp = new_iloc_list();
					tmp =	createCode($$);
					$$->code = tmp;

				}

				|
			 '-' SUB_EXPRESSION %prec OPPREC { $$ = tree_make_unary_node(make_node_val(NULL, AST_ARIM_INVERSAO), $2);
			 coercion(IKS_BOOL, $2);
			 $$->semanticType = IKS_BOOL;
			 iloc_list_t* tmp = new_iloc_list();
			 tmp =	createCode($$);
			 $$->code = tmp;
		 }
				| '!' SUB_EXPRESSION { $$ = tree_make_unary_node(make_node_val(NULL, AST_LOGICO_COMP_NEGACAO), $2);
						coercion(IKS_BOOL, $2);
						$$->semanticType = IKS_BOOL;
						iloc_list_t* tmp = new_iloc_list();
						tmp =	createCode($$);
						$$->code = tmp;
					};
	SUB_EXPRESSION : EXPRESSION_LEAF { $$ = $1; $$->semanticType = $1->semanticType; }
					| '(' EXPRESSION ')' { $$ = $2; $$->semanticType = $2->semanticType; };
