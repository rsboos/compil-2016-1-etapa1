#ifndef __LIST_H
#define __LIST_H

#include "cc_tree.h"
#include "cc_ast.h"

#define GLOBAL_SCOPE 0
#define LOCAL_SCOPE 1

typedef struct iloc_list_item {
    char* opcode;
    int op1;
    int op2;
	int op3;
    struct iloc_list_item *next;
} iloc_list_item_t;

typedef struct iloc_list {
    iloc_list_item_t* first;
    iloc_list_item_t* last;
} iloc_list_t;

iloc_list_item_t* new_iloc_item(char*, int, int, int);
iloc_list_t* new_iloc_list();
void addInstructionToList(iloc_list_t* list, iloc_list_item_t* item);
void addInstructionToListBeforeLast(iloc_list_t* list, iloc_list_item_t* item);
iloc_list_t* createCode(comp_tree_t* node);
iloc_list_t* connectLists(iloc_list_t* l1, iloc_list_t* l2);
iloc_list_t* InstArithmetic(comp_tree_t* node, char* opcode);
iloc_list_t* InstLiteral(comp_tree_t* node);
iloc_list_t* InstBools(comp_tree_t* node);
iloc_list_t* InstLogical(comp_tree_t* node);
iloc_list_t* InstSingleBool(comp_tree_t* node);
iloc_list_t* InstAssignment(comp_tree_t* node);
int calcVectorOffsetReg(comp_tree_t* node, iloc_list_t *il);
iloc_list_t* loadSingleVar(comp_tree_t* node);
iloc_list_t* loadVector(comp_tree_t* node);
iloc_list_t* InstIf(comp_tree_t* node);
iloc_list_t* InstDoWhile(comp_tree_t* node);
iloc_list_t* InstWhile(comp_tree_t* node);
int regName();
int labelName();
void iloc_list_debug_print(iloc_list_t*);
void writeIlocProgram(iloc_list_t* l);
#endif
