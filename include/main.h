/*
  main.h

  Cabeçalho principal do analisador sintático
*/
#ifndef __MAIN_H
#define __MAIN_H
#include <stdio.h>
#include "cc_dict.h"
#include "cc_list.h"
#include "cc_tree.h"
#include "parser.h"
#include "cc_misc.h"

/*
  Constantes a serem utilizadas como valor de retorno no caso de
  sucesso (SINTATICA_SUCESSO) e erro (SINTATICA_ERRO) do analisador
  sintático.
*/
#define SINTATICA_SUCESSO 0
#define SINTATICA_ERRO    1

/*
  Constantes a serem utilizadas para diferenciar os tipos de declaraçoes
*/
#define ALL_TYPES 0
#define VARIABLE_DECLARATION 1
#define FUNCTION_DECLARATION 2
#define ARRAY_DECLARATION    3

void cc_dict_etapa_1_print_entrada (char *token, int line);
int yylex();
char *getenv();
#endif
