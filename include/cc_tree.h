// Copyright (c) 2016 Lucas Nodari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CC_TREE_H_
#define CC_TREE_H_

#include "cc_dict.h"
#include "cc_gv.h"

typedef struct iloc_list iloc_list_t;

/*
 * Tipo: node_value_t, Ã© o tipo usado no campo void value de comp_tree_t
 * itemContent Ã© um ponteiro para a tabela de simbolos
 * type indica o tipo do no conforme cc_ast.h
 */
typedef struct node_value {
	struct comp_dict_item *item;
	int type;
} node_value_t;

/*
 * Tipo: comp_tree_t, value Ã© um tipo genÃ©rico armazenado em cada
 * nÃ³ da Ã¡rvore, childnodes Ã© a quantidade de nÃ³s filhos de um nÃ³,
 * first e last referem-se ao primeiro e ultimo nÃ³s filhos,
 * next e prev percorrem uma lista de nÃ³s filhos de um mesmo nÃ³.
 * Para uma Ã¡rvore com 2 filhos, first->next == last e first == last->prev
 */
typedef struct comp_tree {
	int semanticType;
	int size;
	int regVal;
	iloc_list_t *code;
	param_list_t* paramList;
	int coerce;				// Defines if node have to be coerced (1) or not (0)
	node_value_t *value;
	int childnodes;
	struct comp_tree *first,*last;
	struct comp_tree *next,*prev;
} comp_tree_t;

/* FunÃ§Ãµes: a seguir segue a lista de funÃ§Ãµes da API cc_tree */

/*
 * Função: make_node_val, cria um item do tipo node_value_t com o
 * tipo e o ponteiro para a tabela de simbolos passados por parametro
 * e retorna um ponteiro para esse item.
 */
node_value_t* make_node_val(comp_dict_item_t*, int);

/*
 * FunÃ§Ã£o: tree_new, cria um nÃ³ raiz para a Ã¡rvore. Retorna um
 * ponteiro para o nÃ³ criado ou aborta a execuÃ§Ã£o do programa
 * caso algum erro de alocaÃ§Ã£o de memÃ³ria tenha ocorrido.
 * Equivalente Ã  tree_make_node(NULL).
 */
comp_tree_t* tree_new(void);

/*
 * FunÃ§Ã£o: tree_free, percorre recursivamente a estrutura da
 * Ã¡rvore e libera todos comp_tree_t alocados, os elementos value
 * devem ser tratados externamente, previamente ao uso desta funÃ§Ã£o.
 */
void tree_free(comp_tree_t *tree);

/*
 * FunÃ§Ã£o: tree_make_node, cria um novo nÃ³ e retorna o ponteiro para
 * a estrutura comp_tree_t criada. O paramÃªtro value Ã© uma estrutura
 * qualquer sob responsabilidade do usuÃ¡rio.
 */
comp_tree_t* tree_make_node(node_value_t *value);

/*
 * FunÃ§Ã£o: tree_insert_node, insere node no encadeamento de filhos de
 * tree. Aborta o programa se tree ou node forem nulos.
 */
void tree_insert_node(comp_tree_t *tree, comp_tree_t *node);

/*
 * FunÃ§Ã£o: tree_has_child_nodes, retorna 1 se existir algum nÃ³ no
 * encadeamento de nÃ³s filhos de tree, 0 caso contrÃ¡rio.
 */
int tree_has_child_nodes(comp_tree_t *tree);

/*
 * FunÃ§Ã£o: tree_make_unary_node, cria um novo nÃ³ usando value, e
 * insere node no encadeamento de filhos do novo nÃ³.
 */
comp_tree_t* tree_make_unary_node(node_value_t *value, comp_tree_t* node);

/*
 * FunÃ§Ã£o: tree_make_binary_node, cria um novo nÃ³ usando value, e
 * insere node1 e node2 no encadeamento de filhos do novo nÃ³.
 */
comp_tree_t* tree_make_binary_node(node_value_t *value, comp_tree_t* node1, comp_tree_t* node2);

/*
 * FunÃ§Ã£o: tree_make_ternary_node, cria um novo nÃ³ usando value, e
 * insere node1, node2 e node3 no encadeamento de filhos do novo nÃ³.
 */
comp_tree_t* tree_make_ternary_node(node_value_t *value, comp_tree_t* node1, comp_tree_t* node2, comp_tree_t* node3);

/*
 * FunÃ§Ã£o: tree_debug_print, percorre a Ã¡rvore recursivamente e
 * exibe os ponteiros dos atributos value.
 */
void tree_debug_print(comp_tree_t *tree);


/*
 * Função: typeSystem, recebe dois nodos e retorna um tipo conforme sistema de inferencias da linguagem.
 * É feita anotaçao de coerçao nos nodos caso seja necessario.
 */
int typeSystem(comp_tree_t* t1, comp_tree_t* t2);

/*
 * Função: coercion, recebe um tipo e um nodo, anota que o nodo deve sofrer coerçao ou retorna erro se a coerçao for impossivel
 */
void coercion(int expectedType, comp_tree_t* t);

void verifyReturn(int expectedType, comp_tree_t* t);

// Funçao que recebe um nodo da AST e retorna o lexema associado a ele
char* lexemeNode(comp_tree_t* node);

#endif //CC_TREE_H_
