#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>

int getLineNumber (void);
void yyerror (char const *mensagem);
void main_init (int argc, char **argv);
void main_finalize (void);
void comp_print_table (void);
void cc_dict_etapa_2_print_entrada (char *token, int line, int tipo);
int comp_get_line_number (void);
#endif
