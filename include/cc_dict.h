// Copyright (c) 2016 Lucas Nodari
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef CC_DICT_H_
#define CC_DICT_H_

/*
 * Constante: DICT_SIZE, representa o tamanho de uma tabela de símbolos
 */
#define DICT_SIZE 10240

#define IKS_INT 1
#define IKS_FLOAT 2
#define IKS_CHAR 3
#define IKS_STRING 4
#define IKS_BOOL 5

#define IKS_SUCCESS 0 					//caso nao houver nenhum tipo de erro
/* Verificaçao de declaraçoes */
#define IKS_ERROR_UNDECLARED 1 			//identificador nao declarado
#define IKS_ERROR_DECLARED 2 			//identificador ja declarado
/* Uso correto de identificadores */
#define IKS_ERROR_VARIABLE 3 			//identificador deve ser utilizado como variavel
#define IKS_ERROR_VECTOR 4 				//identificador deve ser utilizado como vetor
#define IKS_ERROR_FUNCTION 5 			//identificador deve ser utilizado como funçao
/* Tipos e tamanho de dados */
#define IKS_ERROR_WRONG_TYPE 6 			//tipos incompatıveis
#define IKS_ERROR_STRING_TO_X 7 		//coerçao impossıvel do tipo string
#define IKS_ERROR_CHAR_TO_X 8 			//coerçao impossıvel do tipo char
/* Argumentos e parametros */
#define IKS_ERROR_MISSING_ARGS 9 		//faltam argumentos
#define IKS_ERROR_EXCESS_ARGS 10 		//sobram argumentos
#define IKS_ERROR_WRONG_TYPE_ARGS 11 	//argumentos incompatıveis
/* Verificaçao de tipos em comandos */
#define IKS_ERROR_WRONG_PAR_INPUT 12 	//parametro nao é identificador
#define IKS_ERROR_WRONG_PAR_OUTPUT 13 	//parametro nao é literal string ou expressao
#define IKS_ERROR_WRONG_PAR_RETURN 14

/*
  Constantes a serem utilizadas para diferenciar os lexemas que estão
  registrados na tabela de símbolos.
*/
#define SIMBOLO_LITERAL_INT    1
#define SIMBOLO_LITERAL_FLOAT  2
#define SIMBOLO_LITERAL_CHAR   3
#define SIMBOLO_LITERAL_STRING 4
#define SIMBOLO_LITERAL_BOOL   5
#define SIMBOLO_IDENTIFICADOR  6

typedef struct param_list {
  int type;
  struct param_list *next;
} param_list_t;

typedef struct comp_dict_item_key {
  char *lexeme;
  int type;
} comp_dict_item_key_t;

typedef struct comp_dict_item_content {
  int lastLine;
  int type;		// Tipo do lexema: SIMBOLO_LITERAL_INT, SIMBOLO_LITERAL_FLOAT, SIMBOLO_LITERAL_CHAR, SIMBOLO_LITERAL_STRING, SIMBOLO_LITERAL_BOOL ou SIMBOLO_IDENTIFICADOR
  int typeVar;		// Tipo da declaraçao (usado se SIMBOLO_IDENTIFICADOR): VARIABLE_DECLARATION, FUNCTION_DECLARATION ou ARRAY_DECLARATION
  int typeContent;	// Tipo do conteudo (usado se SIMBOLO_IDENTIFICADOR)
  int size;		// Tamanho (usado se SIMBOLO_IDENTIFICADOR)
  param_list_t* paramList;
  int scope;
  int offset;
  union {
		int intValue;
		float floatValue;
		char charValue;
		char* stringValue;
		int boolValue;
		char* identificadorValue;
		} value;
} comp_dict_item_content_t;

/*
 * Tipo: comp_dict_item_t, é o tipo de uma entrada na tabela de
 * símbolos. O valor do usuário é registrado no campo _value_, de tipo
 * _void *_. Sendo assim, o usuário pode registrar um ponteiro para
 * qualquer tipo de dados, sendo efetivamente uma entrada genérica. A
 * chave _key_ é o identificador único da entrada. O ponteiro _next_ é
 * utilizado casa exista um conflito na função de hash utilizada.
 */
typedef struct comp_dict_item {
  struct comp_dict_item_key *key;
  struct comp_dict_item_content *content;
  struct comp_dict_item *next;  // ponteiro de overflow
} comp_dict_item_t;

/*
 * Tipo: comp_dict_t, é o tipo da tabela de símbolos. O campo _size_
 * indica o tamanho total, inicializado para DICT_SIZE e depois não é
 * mais mudado. O campo _occupation_ indica a ocupação atual da
 * tabela, indicando quantos elementos ela contém. O campo _data_ é
 * uma matriz de ponteiros que guarda as entradas.
 */
typedef struct comp_dict {
  int size;
  int occupation;
  comp_dict_item_t **data;
} comp_dict_t;

/* Funções: a seguir segue a lista de funções da API cc_dict */

/*
 * Função: dict_new, cria uma nova tabela de símbolos. Retorna um
 * ponteiro para a nova tabela de símbolos ou aborta a execução do
 * programa caso algum erro de alocação de memória tenha ocorrido.
 */
comp_dict_t *dict_new();
void dict_new2(comp_dict_t *dict);

/*
 * Função: dict_free, libera a tabela de símbolos alocada previamente
 * com dict_new. Para evitar vazamentos de memória (verifique com
 * valgrind), é importante lembrar que todos as entradas na tabela de
 * símbolos devem ser removidas e liberadas. Caso a ocupação não seja
 * zero, esta função aborta a execução do programa.
 */
void dict_free(comp_dict_t * dict);

/*
 * Função: dict_put, insere uma nova entrada na tabela de
 * símbolos. Recebe três parâmetros: o parâmetro _dict_ é um ponteiro
 * para a tabela de símbolos na qual será inserida a nova entrada
 * (este ponteiro deve ser obrigatoriamente um retornado pela função
 * dict_new); o parâmetro _key_ é a chave da nova entrada na tabela de
 * símbolos; enfim, o parâmetro _value_ é um ponteiro para qualquer
 * tipo ou estrutura de dados, sob responsabilidade do usuário. Esse
 * ponteiro será associado a chave na tabela de símbolos. Caso
 * qualquer um dos parâmetros seja NULL, a função aborta a execução do
 * programa.
 */
void *dict_put(comp_dict_t * dict, comp_dict_item_key_t *itemKey, comp_dict_item_content_t *itemContent);

int contentSize(int, int, int);
int contSize(int typeContent);
void *insertInSymbolTable(comp_dict_t *dict, int, int, int, char*, int);

/*
 * Função: dict_get, obtém o valor de uma entrada na tabela de
 * símbolos. Recebe dois parâmetros: o parâmetro _dict_ é um ponteiro
 * para a tabela de símbolos da qual será obtida a entrada (este
 * ponteiro deve ser obrigatoriamente um retornado pela função
 * dict_new); o parâmetro _key_ é a chave da entrada na tabela de
 * símbolos. A função retorna um ponteiro que foi informado no momento
 * do dict_put. É normal fazer um cast para o tipo correto
 * imediatamente após essa chamada de função.  Caso qualquer um dos
 * parâmetros seja NULL, a função aborta a execução do programa.
 */
comp_dict_item_t *dict_get(comp_dict_t * dict, comp_dict_item_key_t *key);

/*
 * Função: dict_remove, remove o valor de uma entrada na tabela de
 * símbolos. Recebe dois parâmetros: o parâmetro _dict_ é um ponteiro
 * para a tabela de símbolos da qual será obtida a entrada (este
 * ponteiro deve ser obrigatoriamente um retornado pela função
 * dict_new); o parâmetro _key_ é a chave da entrada na tabela de
 * símbolos que será removida. O valor retorno é o ponteiro que foi
 * informado no momento do dict_put, e deve ser liberado pelo usuário.
 * Caso qualquer um dos parâmetros seja NULL, a função aborta a
 * execução do programa.
 */
void *dict_remove(comp_dict_t * dict, comp_dict_item_key_t *key);

/*
 * Função: dict_debug_print, usada somente para visualizar os
 * ponteiros do dict.
 */
void dict_debug_print(comp_dict_t * dict);

/*
 * Função: addParam, adicionada um parametro na lista de parametros.
 */
param_list_t* addParam(param_list_t*, int);

void param_list_debug_print(param_list_t *l);
void param_list_free(param_list_t *l);

int param_list_get_size(param_list_t *l);

void compareParamList(param_list_t *input, param_list_t *expected);
#endif                          //CC_DICT_H_
