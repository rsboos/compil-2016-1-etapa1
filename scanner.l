/*
Grupo 04
Luis Felipe Polo
Rodrigo Augusto Boos
*/
%{
#include "parser.h" //arquivo automaticamente gerado pelo bison
#include "cc_dict.h"
#include "main.h"
#include "cc_tree.h"
#include "cc_stack.h"
int lineNumber = 1;
struct comp_dict* symbolTable;
stackT s;
%}

%x IN_COMMENT

%%
[ \t]+ { }
\n			{ lineNumber++; }
\/\/.*\n	{ lineNumber++; }

int 		return TK_PR_INT;
float 		return TK_PR_FLOAT;
bool 		return TK_PR_BOOL;
char 		return TK_PR_CHAR;
string 		return TK_PR_STRING;
if 			return TK_PR_IF;
then 		return TK_PR_THEN;
else 		return TK_PR_ELSE;
while 		return TK_PR_WHILE;
do 			return TK_PR_DO;
input 		return TK_PR_INPUT;
output 		return TK_PR_OUTPUT;
return 		return TK_PR_RETURN;
const 		return TK_PR_CONST;
static 		return TK_PR_STATIC;
foreach 	return TK_PR_FOREACH;
for 		return TK_PR_FOR;
switch 		return TK_PR_SWITCH;
case		return TK_PR_CASE;
break 		return TK_PR_BREAK;
continue 	return TK_PR_CONTINUE;
class 		return TK_PR_CLASS;
private 	return TK_PR_PRIVATE;
public 		return TK_PR_PUBLIC;
protected 	return TK_PR_PROTECTED;
\<\= 		return TK_OC_LE;
\>\= 		return TK_OC_GE;
\=\= 		return TK_OC_EQ;
\!\= 		return TK_OC_NE;
\&\& 		return TK_OC_AND;
\|\| 		return TK_OC_OR;
\>\> 		return TK_OC_SR;
\<\< 		return TK_OC_SL;
\,|\;|\:|\(|\)|\[|\]|\{|\}|\+|\-|\*|\/|\<|\>|\=|\!|\&|\$|\%|\#|\^ return yytext[0];
false 					{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							itemKey->lexeme = strdup(yytext);
							itemKey->type = SIMBOLO_LITERAL_BOOL;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_LITERAL_BOOL;
							itemContent->value.boolValue = 0;
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_LIT_FALSE;
						}
true 					{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							itemKey->lexeme = strdup(yytext);
							itemKey->type = SIMBOLO_LITERAL_BOOL;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_LITERAL_BOOL;
							itemContent->value.boolValue = 1;
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_LIT_TRUE;
						}
[0-9]+					{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							itemKey->lexeme = strdup(yytext);
							itemKey->type = SIMBOLO_LITERAL_INT;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_LITERAL_INT;
							itemContent->value.intValue = atoi(yytext);
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_LIT_INT;
						}
[0-9]*\.[0-9]+			{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							itemKey->lexeme = strdup(yytext);
							itemKey->type = SIMBOLO_LITERAL_FLOAT;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_LITERAL_FLOAT;
							itemContent->value.floatValue = atof(yytext);
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_LIT_FLOAT;
						}
[a-zA-Z_][a-zA-Z0-9_]*	{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							itemKey->lexeme = strdup(yytext);
							itemKey->type = SIMBOLO_IDENTIFICADOR;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_IDENTIFICADOR;
							itemContent->value.identificadorValue = strdup(yytext);
							itemContent->paramList = NULL;
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_IDENTIFICADOR;
						}
'.'						{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							char c[1];
							c[0] = yytext[1];
							itemKey->lexeme = strdup(c);
							itemKey->type = SIMBOLO_LITERAL_CHAR;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_LITERAL_CHAR;
							itemContent->value.charValue = yytext[1];
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_LIT_CHAR;
						}
\"[^\"]*\" 				{
							comp_dict_item_t* item;
							comp_dict_item_key_t* itemKey = (comp_dict_item_key_t *)malloc(sizeof(comp_dict_item_key_t));
							comp_dict_item_content_t* itemContent = (comp_dict_item_content_t *)malloc(sizeof(comp_dict_item_content_t));
							char s[yyleng];
							strcpy(s, yytext+1);
							s[yyleng-2]='\0';
							itemKey->lexeme = strdup(s);
							itemKey->type = SIMBOLO_LITERAL_STRING;
							itemContent->lastLine = lineNumber;
							itemContent->type = SIMBOLO_LITERAL_STRING;
							itemContent->value.stringValue = strdup(s);
							item = dict_put(symbolTable, itemKey, itemContent);
							yylval.valor_simbolo_lexico = item;
							return TK_LIT_STRING;
						}
"/*"              BEGIN(IN_COMMENT);
<IN_COMMENT>{
     "*/"      BEGIN(INITIAL);
     [^*\n]+
     "*"
     \n        lineNumber++;
}

. 			return TOKEN_ERRO;
%%
